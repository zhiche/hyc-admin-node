/**
 * Created by Tuffy on 16/8/22.
 */
'use strict';

var sessionUtils = require('../utils/session-utils');

// 配置
var options = {
    // 白名单列表
    permitList: ['/login/**'],
    // 默认路径
    defaultUrl: '/login'
};

// 检测白名单
function isPermit(req) {
    var urlObj = {
        url: req.url
    };
    var url = req.url;
    var index = url.indexOf('?');
    url = url.substr(0, index > 0 ? index : url.length);
    var path = req.query.url || url;
    // 默认路径直接放过
    if (path === options.defaultUrl) {
        return true;
    }
    // 过滤白名单列表
    for(var i = 0; i < options.permitList.length; i++) {
        var item = options.permitList[i];
        if (item.indexOf('/**') >= 0) {
            var replaceStr = item.replace(/\/\*\*/g, '');
            if (path.indexOf(replaceStr) === 0) {
                return true;
            }
        } else if (item === path) {
            return true;
        }
    }
    // 是否存在token
    if (sessionUtils.getToken(req)) {
        return true;
    }
    if (!(urlObj.url.indexOf('.ico') > 0
        || urlObj.url.indexOf('.css') > 0
        || urlObj.url.indexOf('.js') > 0
        || urlObj.url.indexOf('.html') > 0
        || urlObj.url.indexOf('.jpg') > 0
        || urlObj.url.indexOf('.jpeg') > 0
        || urlObj.url.indexOf('.png') > 0
        || urlObj.url.indexOf('.gif') > 0)) {
        sessionUtils.setRedirect(req, urlObj.url);
    }
    return false;
};

// 导出对象
module.exports = function appSecurity(req, res, callback) {
    if (isPermit(req)) {
        callback();
        return;
    }
    res.redirect(options.defaultUrl);
};