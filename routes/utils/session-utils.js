/**
 * Created by Tuffy on 16/8/22.
 */
'use strict';

// session token
var TOKEN_IN_SESSION = 'TOKEN_IN_SESSION';

// 系统间票据凭证
var BASE_TOKEN_IN_SESSION = 'BASE_TOKEN_IN_SESSION';

// 重定向地址
var REDIRECT_IN_SESSION = 'REDIRECT_IN_SESSION';

// 导出session对象
module.exports = sessionUtils;

// 声明session对象
function sessionUtils() {};

/**
 * 获取sesson对象
 * @param request 请求
 * @returns {*} session对象
 */
sessionUtils.getSession = function (req) {
    return req.session;
};

/**
 * 将token写入session
 * @param req 请求对象
 * @param token token
 */
sessionUtils.setToken = function (req, token) {
    sessionUtils.getSession(req)[TOKEN_IN_SESSION] = token;
};


/**
 * 获取session中token信息
 * @param req 请求对象
 * @returns {*} token信息
 */
sessionUtils.getToken = function (req) {
    return sessionUtils.getSession(req)[TOKEN_IN_SESSION];
};

/**
 * 设置系统间票据凭证
 * @param req 请求对象
 * @param token token信息
 */
sessionUtils.setBaseToken = function (req, token) {
    sessionUtils.getSession(req)[BASE_TOKEN_IN_SESSION] = token;
};

/**
 * 获取系统间票据凭证
 * @param req 请求对象
 * @returns {*} token信息
 */
sessionUtils.getBaseToken = function (req) {
    return sessionUtils.getSession(req)[BASE_TOKEN_IN_SESSION];
};

/**
 * 将重定向url写入session
 * @param req 请求对象
 * @param redirect 重定向url
 */
sessionUtils.setRedirect = function (req, redirect) {
    sessionUtils.getSession(req)[REDIRECT_IN_SESSION] = redirect;
};

/**
 * 获取session中重定向url信息
 * @param req 请求对象
 * @returns {*} 重定向url信息
 */
sessionUtils.getRedirect = function (req) {
    return sessionUtils.getSession(req)[REDIRECT_IN_SESSION];
};

