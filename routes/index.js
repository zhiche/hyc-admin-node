var express = require('express');
var router = express.Router();

var baseConfig = require('./resource/base-config');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        isCompressed: baseConfig.compressed,
        version: baseConfig.version
    });
});

module.exports = router;
