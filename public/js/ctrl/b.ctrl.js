/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('BannerCtrl', ['$rootScope', '$scope', 'BannerService', function ($rootScope, $scope, BannerService) {

    // banner对象
    $scope.bannerObj = {
        list: [],
        init: function () {
            var that = this;
            BannerService.list()
                .success(function (result) {
                    if (result.success) {
                        that.list = result.data;
                    }
                })
        }
    };

    // 初始化
    $scope.bannerObj.init();
}])

.controller('BannerEditCtrl', ['$rootScope', '$scope', '$state', 'BannerService', function ($rootScope, $scope, $state, BannerService) {

    // banner对象
    $scope.bannerObj = {
        info: {},
        byId: function () {
            var that = this;
            var id = $state.params.id;
            if (id) {
                BannerService.byId(id)
                    .success(function (result) {
                        if (result.success) {
                            that.info = result.data;
                        }
                    });
            }
        },
        save: function () {
            BannerService.save({
                id: $scope.bannerObj.info.id,
                title: $scope.bannerObj.info.title,
                icon: $scope.bannerObj.info.icon,
                sort: $scope.bannerObj.info.sort
            })
                .success(function (result) {
                    if (result.success) {
                        layer.msg('编辑成功');
                        history.go(-1);
                    } else {
                        layer.msg(result.message);
                    }
                })
        }
    };

    // 初始化
    $scope.bannerObj.byId();
}])

.controller('BillDriverCtrl', ['$rootScope', '$scope', '$state', 'BillService', 'BillDetailsService', function ($rootScope, $scope, $state, BillService, BillDetailsService) {

	$scope.page = {
	        pageNo: 1,
	        totalPage: 0
	};
	
	// 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            driverlist();
        }
    };
    
    // 初始化数据列表
    function loadData() {
        OrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.orderInfo;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }
    
    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }
    
    // 查询司机账单
    function driverlist(){
    	BillService.driverList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result){
			if(result.success){
				$scope.cBills = result.data.cBills;
				$scope.page = result.data.page;
				buildPage();
			} else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
			}
    	})
    }

    driverlist();
}])

.controller('BillStanCompanyCtrl', ['$rootScope', '$scope', '$state', 'BillService', function ($rootScope, $scope, $state, BillService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            stancompanylist();
        }
    };

    // 初始化数据列表
    function loadData() {
        OrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.orderInfo;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 查询司机账单
    function stancompanylist(){
        BillService.stancompanyList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result){
            if(result.success){
                $scope.cBills = result.data.cBills;
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    }
    $scope.statement = function (item) {
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        BillService.updateStanCompanyStatus({
            orderId: item.id
        }).success(function (result) {
            if (result.success) {
                stancompanylist();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        }).finally(function () {
            $rootScope.hycadmin.loading.hide();
        });
    }

    stancompanylist();
}])


.controller('BillKyleownerCtrl', ['$rootScope', '$scope', '$state', 'BillService', function ($rootScope, $scope, $state, BillService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            kyleownerlist();
        }
    };

    // 初始化数据列表
    function loadData() {
        OrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.orderInfo;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 查询司机账单
    function kyleownerlist(){
        BillService.kyleownerList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result){
            if(result.success){
                $scope.cBills = result.data.cBills;
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    }

    kyleownerlist();
}])

.controller('BillKyleCompanyCtrl', ['$rootScope', '$scope', '$state', 'BillService', function ($rootScope, $scope, $state, BillService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            kylecompanylist();
        }
    };

    // 初始化数据列表
    function loadData() {
        OrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.orderInfo;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 查询司机账单
    function kylecompanylist(){
        BillService.kylecompanyList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result){
            if(result.success){
                $scope.cBills = result.data.cBills;
                angular.forEach($scope.cBills, function (item) {
                    item.amount =  Math.abs(item.amount);
                });
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    };

    // 结算
    $scope.statement = function (item) {
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        var cost = Math.abs(item.amount) - Math.abs(item.scoresPaid);
        BillService.statement({
            orderId: item.id,
            userId: item.userId,
            userType: item.userType,
            cost: cost
        }).success(function (result) {
                if (result.success) {
                    kylecompanylist();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            }).finally(function () {
            $rootScope.hycadmin.loading.hide();
        });
    }

    kylecompanylist();
}])

.controller('BankCardCtrl', ['$rootScope', '$scope', '$state', 'BankCardCtrlService', function ($rootScope, $scope, $state, BankCardCtrlService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    $scope.bank = {
        realName: '',
        phone: '',
        bankName: ''
    };
    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        BankCardCtrlService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            realName: $scope.bank.realName,
            phone: $scope.bank.phone,
            bankName: $scope.bank.bankName
        }).success(function (result){
            if(result.success){
                $scope.usercards = result.data.usercards;
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.bank = {
            realName: '',
            phone: '',
            bankName: ''
        };
        $scope.loadData('load');
    };

   /*初始化数据列表
   $scope.loadData = function () {
       BankCardCtrlService.list({
           pageNo: $scope.userTruckObj.pageNo,
           pageSize: $scope.userTruckObj.pageSize
      }).success(function (result){
           if(result.success){
               $scope.usercards = result.data.usercards;
               $scope.page = result.data.page;
               buildPage();
           } else{
               $rootScope.hycadmin.toast({
                   title: result.message,
                   timeOut: 3000
               });
           }
       })
   };*/

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    $scope.loadData();
}])

.controller('BillDriverCtrl', ['$rootScope', '$scope', '$state', 'BilldriverService', 'BillSendbydriverService', function ($rootScope, $scope, $state, BilldriverService, BillSendbydriverService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    $scope.cBill = {
        userName: null,
        phone: null,
        status: null,
        userType: 10
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData('load');
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        BillSendbydriverService.sendbydriverList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,

            userName: $scope.cBill.userName,
            phone: $scope.cBill.phone,
            status: $scope.cBill.status,
            userType: $scope.cBill.userType
        }).success(function (result){
            if(result.success){
                $scope.cBills = result.data.cBills;
                angular.forEach($scope.cBills, function (item) {
                    item.amount =  Math.abs(item.amount);
                });
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.cBill = {
            userName: null,
            phone: null,
            status: null,
            userType: 10
        };
        $scope.loadData('load');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 结算
    $scope.settlement = function (id) {
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        BilldriverService.settlement({
            id: id
        }).success(function (result) {
            if (result.success) {
                $scope.loadData("load");
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        }).finally(function () {
            $rootScope.hycadmin.loading.hide();
        });
    }

    $scope.loadData("load");
}])

.controller('BillSendByDriverCtrl', ['$rootScope', '$scope', '$state', '$modal', 'BillSendbydriverService', function ($rootScope, $scope, $state, $modal, BillSendbydriverService) {

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    $scope.types = [{
        code: '0',
        name: '奖'
    },{
        code: '1',
        name: '罚'
    }];

    $scope.cturnObj = {
        id: '',
        money: '',
        remark: '',
        type: ''
    };

    var turnoverModal = $modal({scope: $scope, title: '奖罚', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/reward-punish-view.html', show: false});

    $scope.cBill = {
        userName: '',
        phone: '',
        status: '',
        beginDateParam: '',
        endDateParam: '',
        userType: 50
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData('load');
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        $scope.cBill.beginDateParam = $scope.cBill.beginDateParam == ''? "": typeof($scope.cBill.beginDateParam) == 'string'? $scope.cBill.beginDateParam : $scope.cBill.beginDateParam.format("yyyy-MM-dd HH:mm:ss");
        $scope.cBill.endDateParam = $scope.cBill.endDateParam == ''? "": typeof($scope.cBill.endDateParam) == 'string'? $scope.cBill.endDateParam : $scope.cBill.endDateParam.format("yyyy-MM-dd HH:mm:ss");
        BillSendbydriverService.sendbydriverList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            userName: $scope.cBill.userName,
            phone: $scope.cBill.phone,
            beginDateParam: $scope.cBill.beginDateParam,
            endDateParam: $scope.cBill.endDateParam,
            status: $scope.cBill.status,
            userType: 50
        }).success(function (result){
            if(result.success){
                $scope.cBills = result.data.cBills;
                angular.forEach($scope.cBills, function (item) {
                    item.amount =  Math.abs(item.amount);
                });
                $scope.page = result.data.page;
                buildPage();
            } else{
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.cBill = {
            userName: '',
            phone: '',
            status: '',
            beginDateParam: '',
            endDateParam: '',
            userType: 50
        };
        $scope.loadData('load');
    };

    //导出Execl表格
    /*$scope.exportExcel = function(params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        $scope.cBill.beginDateParam = $scope.cBill.beginDateParam == ''? "": typeof($scope.cBill.beginDateParam) == 'string'? $scope.cBill.beginDateParam : $scope.cBill.beginDateParam.format("yyyy-MM-dd HH:mm:ss");
        $scope.cBill.endDateParam = $scope.cBill.endDateParam == ''? "": typeof($scope.cBill.endDateParam) == 'string'? $scope.cBill.endDateParam : $scope.cBill.endDateParam.format("yyyy-MM-dd HH:mm:ss");
        BillSendbydriverService.exportExcel({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            userName: $scope.cBill.userName,
            phone: $scope.cBill.phone,
            beginDateParam: $scope.cBill.beginDateParam,
            endDateParam: $scope.cBill.endDateParam,
            status: $scope.cBill.status,
            userType: 50
        }).success(function(result){
            if(result.success){

            }else{

            }
        })
    };*/

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    $scope.showDlg = function (id) {
        $scope.cturnObj = {
            id: '',
            money: '',
            remark: '',
            type: ''
        };
        $scope.cturnObj.id = id;
        turnoverModal.$promise.then(turnoverModal.show);
    }

    // 结算
    $scope.settlement = function (id) {
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        BillSendbydriverService.settlement({
            id: id
        }).success(function (result) {
            if (result.success) {
                $scope.loadData("load");
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        }).finally(function () {
            $rootScope.hycadmin.loading.hide();
        });
    }
    // 奖罚
    $scope.rewardandpunish = function () {
        if ($scope.cturnObj.type == '') {
            $rootScope.hycadmin.toast({
                title: '请设置奖罚类型',
                timeOut: 3000
            });
            return;
        }
        else if ($scope.cturnObj.money == '') {
            $rootScope.hycadmin.toast({
                title: '奖罚金额不能为空',
                timeOut: 3000
            });
            return;
        }
        else if ($scope.cturnObj.money <= 0) {
            $rootScope.hycadmin.toast({
                title: '奖罚金额为不为零的正数',
                timeOut: 3000
            });
            return;
        }
        else if (!angular.isNumber($scope.cturnObj.money)) {
            $rootScope.hycadmin.toast({
                title: '奖罚金额格式错误',
                timeOut: 3000
            });
            return;
        }
        BillSendbydriverService.rewardandpunish({
                id: $scope.cturnObj.id,
                money: $scope.cturnObj.money,
                remark: $scope.cturnObj.remark,
                type: $scope.cturnObj.type
        }
        ).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: '奖罚金额已设置',
                    timeOut: 3000
                });
                $scope.loadData("load");
                turnoverModal.$promise.then(turnoverModal.hide);
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };




    // 条件遍历
    function parseParams4Url() {
        var paramsArray = [];
        for (var i in $scope.cBill) {
            var val = $scope.cBill[i];
            if (val) {
                paramsArray.push(i + '=' + val);
            }
        }
        var exportExcelEle = angular.element(document.getElementById('export_excel'));
        exportExcelEle.attr('href', '/download/excel?url=' + encodeURIComponent('/balance/exportexcel') + '&' + paramsArray.join('&'));
    }

    $scope.loadData("load");

    // 监听属性名
    $scope.$watch('cBill.userName', function () {
        parseParams4Url();
    });
    $scope.$watch('cBill.phone', function () {
        parseParams4Url();
    });
    $scope.$watch('cBill.status', function () {
        parseParams4Url();
    });
    $scope.$watch('cBill.beginDateParam', function () {
        parseParams4Url();
    });
    $scope.$watch('cBill.endDateParam', function () {
        parseParams4Url();
    });
}])

.controller('BillSendbyDriverDetailsCtrl', ['$rootScope', '$scope', '$state', 'BillSendByDriverDetailsService', function ($rootScope, $scope, $state, BillSendByDriverDetailsService) {

    var billId = $state.params.billId;

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            loadData();
        }
    };

    // 初始化数据列表
    function loadData() {
        BillSendByDriverDetailsService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            billId : billId
        }).success(function (result) {
            if (result.success) {
                $scope.cTurnovers = result.data.cTurnovers;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    loadData();
}])

.controller('BillDetailsCtrl', ['$rootScope', '$scope', '$state', 'BillDetailsService', function ($rootScope, $scope, $state, BillDetailsService) {

    var billId = $state.params.billId;

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            loadData();
        }
    };

    // 初始化数据列表
    function loadData() {
        BillDetailsService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            billId : billId
        }).success(function (result) {
            if (result.success) {
                $scope.cTurnovers = result.data.cTurnovers;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    loadData();
}])

.controller('BasisCityLevelCtrl', ['$rootScope', '$scope', '$modal', 'AreaService', function ($rootScope, $scope, $modal, AreaService) {
    // 城市级别
    $scope.levelData = [{
        code: 'C1',
        name: '1级'
    },{
        code: 'C2',
        name: '2级'
    },{
        code: 'C3',
        name: '3级'
    },{
        code: 'C4',
        name: '4级'
    }];

    // 是否可运
    $scope.isTransportData = [{
        code: true,
        name: '可运'
    },{
        code: false,
        name: '不可运'
    }];

    // 区域对象
    $scope.areaObj = {
        proCode: null,
        proName: null,
        cityCode: null,
        cityName: null,
        countyCode: null,
        countyName: null,
        cityRating: null,
        isTransport: null
    };

    // 加载省份
    $scope.loadProvince = [];

    // 加载城市
    $scope.loadCity = [];

    // 区县
    $scope.loadCounty = [];

    // 区域列表
    $scope.areas = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '城市级别调整', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/dialog/basis-city-dlg-view.html', show: false});

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        AreaService.cityRatingList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            proCode: $scope.areaObj.proCode,
            cityCode: $scope.areaObj.cityCode,
            countyCode: $scope.areaObj.countyCode,
            cityRating: $scope.areaObj.cityRating
        }).success(function (result) {
            if (result.success) {
                $scope.areas = result.data.cityRatingList;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };

    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.areaObj.proCode)
            .success(function (result) {
                if (result.success) {
                    $scope.loadCity = result.data;
                }
            });
    };

    // 根据省编码查询发车区县
    $scope.loadDepartCounty = function() {
        AreaService.loadCountyByCode($scope.areaObj.cityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.loadCounty = result.data;
                }
            });
    };

    // 清空查询条件
    $scope.clearParams = function () {
        $scope.areaObj = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        $scope.loadData('load');
    };

    // 显示模态窗口
    $scope.showModel = function () {
        $scope.check = [];
        angular.forEach($scope.areas, function(item) {
            if (item.active) {
                $scope.check.push(item);
            }
        });
        if ($scope.check.length == 1) {
            $scope.areaObj.proName = $scope.check[0].proName;
            $scope.areaObj.cityName = $scope.check[0].cityName;
            $scope.areaObj.countyName = $scope.check[0].countyName;
            myOtherModal.$promise.then(myOtherModal.show);
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择一条数据进行级别调整',
                timeOut: 3000
            });
            return;
        }
    };

    $scope.updateArea = {
        cityRating: null,
        isTransport: null
    };

    // 更新数据
    $scope.updateArea = function () {
        AreaService.updateCityRating({
            proCode: $scope.check[0].proCode,
            cityCode: $scope.check[0].cityCode,
            countyCode: $scope.check[0].countyCode,
            cityRating: $scope.updateArea.cityRating,
            isTransport: $scope.updateArea.isTransport
        }).success(function (result) {
                if (result.success) {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                    myOtherModal.$promise.then(myOtherModal.hide);
                    $scope.loadData('load');
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    }
    loadProvince();
    $scope.loadData('load');
}])

.controller('BasisDistanceCtrl', ['$rootScope', '$scope', '$modal', 'AreaService', 'DistanceService', function ($rootScope, $scope, $modal, AreaService, DistanceService) {

    // 区域对象
    $scope.distanceObj = {
        id: null,
        firstProCode: '',
        firstProName: '',
        firstCityCode: '',
        firstCityName: '',
        secondProCode: '',
        secondProName: '',
        secondCityCode: '',
        secondCityName: '',
        distance: null
    };

    // 加载省份
    $scope.loadProvince = [];

    // 加载城市
    $scope.loadCity = [];

    // 运距
    $scope.distances = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '运距调整', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/dialog/basis-distance-dlg-view.html', show: false});

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        DistanceService.getList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            firstProCode: $scope.distanceObj.firstProCode,
            firstCityCode: $scope.distanceObj.firstCityCode
        }).success(function (result) {
            if (result.success) {
                $scope.distances = result.data.cityDistances;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };

    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.distanceObj.firstProCode)
            .success(function (result) {
                if (result.success) {
                    $scope.loadCity = result.data;
                }
            });
    };

    // 清空查询条件
    $scope.clearParams = function () {
        $scope.distanceObj = {
            id: null,
            firstProCode: '',
            firstProName: '',
            firstCityCode: '',
            firstCityName: '',
            secondProCode: '',
            secondProName: '',
            secondCityCode: '',
            secondCityName: '',
            distance: null
        };
        $scope.loadData('load');
    };

    // 显示模态窗口
    $scope.showModel = function () {
        $scope.check = [];
        angular.forEach($scope.distances, function(item) {
            if (item.active) {
                $scope.check.push(item);
            }
        });
        if ($scope.check.length == 1) {
            $scope.distanceObj.firstProName = $scope.check[0].firstProName;
            $scope.distanceObj.firstCityName = $scope.check[0].firstCityName;
            $scope.distanceObj.secondProName = $scope.check[0].secondProName;
            $scope.distanceObj.secondCityName = $scope.check[0].secondCityName;
            $scope.distanceObj.distance = $scope.check[0].distance;
            myOtherModal.$promise.then(myOtherModal.show);
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择一条数据进行运距调整',
                timeOut: 3000
            });
            return;
        }
    };

    $scope.updateDisObj = {
        distance: ''
    }

    // 更新数据
    $scope.updateDistance = function () {
        if ($scope.updateDisObj.distance == null) {
            $rootScope.hycadmin.toast({
                title: '新运距不能为空',
                timeOut: 3000
            });
            return;
        } else if ((+$scope.updateDisObj.distance) <= 0) {
            $rootScope.hycadmin.toast({
                title: '请输入合法运距',
                timeOut: 3000
            });
            return;
        }
        DistanceService.updateDistance({
            id: $scope.check[0].id,
            distance: $scope.updateDisObj.distance
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
                myOtherModal.$promise.then(myOtherModal.hide);
                $scope.loadData('load');
                $scope.updateDisObj.distance = '';
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }
    loadProvince();
    $scope.loadData('load');
}])

.controller('BasisCoefficientCtrl', ['$rootScope', '$scope', '$modal', 'ShippingService', function ($rootScope, $scope, $modal, ShippingService) {

    // 运距对象
    $scope.coeffObj = {
        id: null,
        beginRange: '',
        endRange: '',
        rate: '',
        subsidy: ''
    };

    // 运距
    $scope.coefficient = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '系数调整', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/dialog/basis-distance-coefficient-dlg-view.html', show: false});

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function () {
        ShippingService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.coefficient = result.data.ShippingFee;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 显示模态窗口
    $scope.showModel = function () {
        $scope.check = [];
        angular.forEach($scope.coefficient, function(item) {
            if (item.active) {
                $scope.check.push(item);
            }
        });
        if ($scope.check.length == 1) {
            $scope.coeffObj.beginRange = $scope.check[0].beginRange;
            $scope.coeffObj.endRange = $scope.check[0].endRange;
            $scope.coeffObj.rate = $scope.check[0].rate;
            myOtherModal.$promise.then(myOtherModal.show);
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择一条数据进行系数调整',
                timeOut: 3000
            });
            return;
        }
    };

    $scope.updateCoeffObj = {
        rate: ''
    }

    // 更新数据
    $scope.updateCoeff = function () {
        if ($scope.updateCoeffObj.rate == null) {
            $rootScope.hycadmin.toast({
                title: '新系数不能为空',
                timeOut: 3000
            });
            return;
        } else if ((+$scope.updateCoeffObj.rate) <= 0) {
            $rootScope.hycadmin.toast({
                title: '请输入合法系数',
                timeOut: 3000
            });
            return;
        }
        ShippingService.updateRate({
            id: $scope.check[0].id,
            rate: $scope.updateCoeffObj.rate
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
                myOtherModal.$promise.then(myOtherModal.hide);
                $scope.loadData();
                $scope.updateCoeffObj.rate = '';
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }
    $scope.loadData();
}])

.controller('BasisFoctorCtrl', ['$rootScope', '$scope', '$modal', 'ShippingService', function ($rootScope, $scope, $modal, ShippingService) {

    // 运距对象
    $scope.foctorObj = {
        id: null,
        beginRange: '',
        endRange: '',
        rate: '',
        subsidy: ''
    };

    // 运距对象
    $scope.foctors = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '浮动费用调整', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/dialog/basis-distance-foctor-dlg-view.html', show: false});

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function () {
        ShippingService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize
        }).success(function (result) {
            if (result.success) {
                $scope.foctors = result.data.ShippingFee;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 显示模态窗口
    $scope.showModel = function () {
        $scope.check = [];
        angular.forEach($scope.foctors, function(item) {
            if (item.active) {
                $scope.check.push(item);
            }
        });
        if ($scope.check.length == 1) {
            $scope.foctorObj.beginRange = $scope.check[0].beginRange;
            $scope.foctorObj.endRange = $scope.check[0].endRange;
            $scope.foctorObj.subsidy = $scope.check[0].subsidy;
            myOtherModal.$promise.then(myOtherModal.show);
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择一条数据进行浮动费用调整',
                timeOut: 3000
            });
            return;
        }
    };

    $scope.updateFoctorObj = {
        subsidy: ''
    }

    // 更新数据
    $scope.updateFoctor = function () {
        if ($scope.updateFoctorObj.subsidy == null) {
            $rootScope.hycadmin.toast({
                title: '新浮动费用不能为空',
                timeOut: 3000
            });
            return;
        } else if ((+$scope.updateFoctorObj.subsidy) < 0) {
            $rootScope.hycadmin.toast({
                title: '请输入合法的浮动费用',
                timeOut: 3000
            });
            return;
        }
        ShippingService.updateSubsidy({
            id: $scope.check[0].id,
            subsidy: $scope.updateFoctorObj.subsidy
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
                myOtherModal.$promise.then(myOtherModal.hide);
                $scope.loadData();
                $scope.updateFoctorObj.subsidy = '';
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }
    $scope.loadData();
}])

.controller('BrandCtrl', ['$rootScope', '$scope', '$modal', 'FileUploader', 'QiniuService', 'BrandService', 'VehicleService', function ($rootScope, $scope, $modal, FileUploader, QiniuService, BrandService, VehicleService) {
    //查询对象
    $scope.brand = {
        brandPrefix: '',
        brandName: '',
        brandNation: ''
    }

    // 品牌对象
    $scope.bVehicleBrand = {
        id: null,
        brandPrefix: '',
        brandName: '',
        brandNation: '',
        brandLogo: '',
        comment: '',
        createTime: '',
        updateTime: ''
    };

    // 品牌列表
    $scope.brands = [];

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '车辆品牌', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/brand-dlg-view.html', show: false});

    $scope.page = {
        pageNo: 1,
        totalPage: 0
    };

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        BrandService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            brandPrefix: $scope.brand.brandPrefix,
            brandName: $scope.brand.brandName,
            brandNation: $scope.brand.brandNation
        }).success(function (result) {
            if (result.success) {
                $scope.brands = result.data.brands;
                $scope.page = result.data.page;
                angular.forEach($scope.brands, function (item) {
                    item.brandLogoHtml = '<img src="' + item.url + '" style="width: 35px; height: 35px;" onerror="Tools.onLoadImgErrorCallback(this, \'/image/truck-default-logo.png\')">';
                    item.brandLogoDlgHtml = '<img src="' + item.url + '" style="width: 160px; height: 120px; position: absolute; z-index: 0;" onerror="Tools.onLoadImgErrorCallback(this, \'/image/truck-default-logo.png\')">';
                });
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 显示模态窗口
    $scope.showModel = function (params) {
        $scope.uploadImgs();
        if ('edit' === params) {
            $scope.check = [];
            angular.forEach($scope.brands, function(item) {
                if (item.active) {
                    $scope.check.push(item);
                }
            });
            if ($scope.check.length == 1) {
                $scope.bVehicleBrand = $scope.check[0];
                myOtherModal.$promise.then(myOtherModal.show);
            } else {
                $rootScope.hycadmin.toast({
                    title: '请选择一条数据',
                    timeOut: 3000
                });
                return;
            }
        } else {
            // 清空对象
            clean();
            myOtherModal.$promise.then(myOtherModal.show);
        }
    };

    // 清空品牌对象
    function clean(){
        $scope.bVehicleBrand = {
            id: null,
            brandPrefix: '',
            brandName: '',
            brandNation: '',
            brandLogo: '',
            comment: '',
            createTime: '',
            updateTime: ''
        };
    };

    // 更新数据
    $scope.updateBrand = function () {
        if ($scope.bVehicleBrand.brandPrefix == null) {
            $rootScope.hycadmin.toast({
                title: '首字母不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.bVehicleBrand.brandName == null) {
            $rootScope.hycadmin.toast({
                title: '品牌名称不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.bVehicleBrand.brandNation == null) {
            $rootScope.hycadmin.toast({
                title: '国家不能为空',
                timeOut: 3000
            });
            return;
        }
        delete $scope.bVehicleBrand.createTime;
        delete $scope.bVehicleBrand.updateTime;
        BrandService.edit($scope.bVehicleBrand)
            .success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
                myOtherModal.$promise.then(myOtherModal.hide);
                // 清空数据
                clean();
                $scope.loadData();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    //清空查询条件
    $scope.clearQuery = function () {
        $scope.brand = {
            brandPrefix: '',
            brandName: '',
            brandNation: ''
        }
        $scope.loadData('load');
    };

    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.file, $scope)
            .then(function(result) {
                $scope.imageSrc = result;
            });
    };

    // 上传LOGO
    $scope.uploadImgs = function () {
        var uploader = $scope.uploader = null;
        QiniuService.getUploadToken()
            .success(function (result) {
                if (result.success) {
                    // LOGO
                    var imgKey = null;
                    var uploader = $scope.uploader = new FileUploader({
                        url: 'http://upload.qiniu.com/',
                        formData: [{
                            token: result.data
                        }],
                        queueLimit: 1,
                        autoUpload: true
                    });

                    uploader.filters.push({
                        name: 'file',
                        fn: function(item /*{File|FileLikeObject}*/, options) {
                            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                        }
                    });
                    uploader.onSuccessItem = function(fileItem, response, status, headers) {
                        $scope.bVehicleBrand.brandLogo = response.key;
                        $rootScope.hycadmin.loading({
                            title: '数据处理中...'
                        });
                        QiniuService.getDownloadTicket({
                                key: response.key
                            }).success(function(result){
                                if (result.success) {
                                    $scope.bVehicleBrand.brandLogoDlgHtml = '<img src="' + result.data + '" style="width: 160px; height: 120px; position: absolute; z-index: 0;" onerror="Tools.onLoadImgErrorCallback(this, \'/image/truck-default-logo.png\')">';
                                }
                            }).finally(function() {
                            $rootScope.hycadmin.loading.hide();
                        });
                    };
                }
            })
    };
    $scope.loadData('load');
}])

.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);