/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl.controller('IndexCtrl', ['$rootScope', '$scope', '$state', '$modal', 'IndexService', function($rootScope, $scope, $state, $modal, IndexService) {

    $scope.dropSelect = [{
        name: '今天',
        code: 'day'
    },{
        name: '一周',
        code: 'week'
    },{
        name: '本月',
        code: 'month'
    }];

    $scope.conditionObj = {
        orderCode: 'day',
        scheduleCode: 'day',
        count: 0,
        amount: 0,
        notSchedule: 0,
        alreadySchedule: 0,
        countAndAmount: function (param) {
            IndexService.countAndAmount({
                params: param
            }).success(function (result) {
                if (result.success) {
                    $scope.conditionObj.orderCode = param;
                    $scope.conditionObj.count = result.data.countOrder || 0;
                    $scope.conditionObj.amount = result.data.sumAmount || 0;
                }
            });
        },
        schedule: function (param) {
            IndexService.schedule({
                params: param
            }).success(function (result) {
                if (result.success) {
                    $scope.conditionObj.scheduleCode = param;
                    $scope.conditionObj.notSchedule = result.data.notSchedule || 0;
                    $scope.conditionObj.alreadySchedule = result.data.alreadySchedule || 0;
                }
            });
        },
        initOrder: function () {
            $scope.conditionObj.countAndAmount($scope.conditionObj.orderCode);
        },
        initServiceOrder: function () {
            $scope.conditionObj.schedule($scope.conditionObj.scheduleCode);
        }
    }
    // 初始化数据
    $scope.conditionObj.initOrder();
    $scope.conditionObj.initServiceOrder();
}])