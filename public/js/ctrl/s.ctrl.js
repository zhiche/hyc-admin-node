/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl
.controller('SplitOrderCtrl', ['$rootScope', '$scope', '$state', '$aside', 'ServiceOrderService', 'AreaService', 'WaybillService', function ($rootScope, $scope, $state, $aside, ServiceOrderService, AreaService, WaybillService) {

    // 查询条件
    $scope.queryOrderinfo = {
        departProvinceCode: '',
        departCityCode: '',
        departCountyCode: '',
        receiptProvinceCode: '',
        receiptCityCode: '',
        receiptCountyCode: '',
        orderCode: '',
        beginCreateTime: '',
        endCreateTime: '',
        sOrderCode: ''
    }

    // 订单列表
    $scope.orderInfo = [];

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 加载发车市
    $scope.departCity = [];

    // 加载到达市
    $scope.receiptCity = [];

    // 运单详情
    $scope.wayBillInfo = [];

    // 初始化详情窗体
    var myOtherAside = $aside({scope: $scope, title: '调度详情', templateUrl: $rootScope.Context.path + '/templates/components/order-waybill.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        if ($scope.queryOrderinfo.beginCreateTime != null && $scope.queryOrderinfo.endCreateTime != null){
            if ((+$scope.queryOrderinfo.endCreateTime) - (+$scope.queryOrderinfo.beginCreateTime) < 0) {
                $rootScope.hycadmin.toast({
                    title: '开始时间不能大于结束时间',
                    timeOut: 3000
                });
                return;
            }
        }
        ServiceOrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            departProvinceCode: $scope.queryOrderinfo.departProvinceCode,
            departCityCode: $scope.queryOrderinfo.departCityCode,
            departCountyCode: $scope.queryOrderinfo.departCountyCode,
            receiptProvinceCode: $scope.queryOrderinfo.receiptProvinceCode,
            receiptCityCode: $scope.queryOrderinfo.receiptCityCode,
            receiptCountyCode: $scope.queryOrderinfo.receiptCountyCode,
            orderCode: $scope.queryOrderinfo.orderCode,
            sOrderCode: $scope.queryOrderinfo.sOrderCode,
            beginCreateTime: $scope.queryOrderinfo.beginCreateTime == ''?"":$scope.queryOrderinfo.beginCreateTime.format("yyyy-MM-dd 00:00:00"),
            endCreateTime: $scope.queryOrderinfo.endCreateTime == ''?"":$scope.queryOrderinfo.endCreateTime.format("yyyy-MM-dd 23:59:59")
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.sorders;
                angular.forEach($scope.orderInfo, function (item) {
                    angular.forEach($rootScope.sorderIds, function (id) {
                        if (item.id == id) {
                            item.active = true;
                        }
                    })
                });
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.queryOrderinfo = {
            departProvinceCode: '',
            departCityCode: '',
            departCountyCode: '',
            receiptProvinceCode: '',
            receiptCityCode: '',
            receiptCountyCode: '',
            orderCode: '',
            beginCreateTime: '',
            endCreateTime: '',
            sOrderCode: ''
        };
        $scope.loadData('load');
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };
    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.departProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCity = result.data;
                }
            });
    };
    // 根据省编码查询收车市
    $scope.loadReceiptCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.receiptProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCity = result.data;
                }
            });
    };

    // 根据市编码查询收车区县
    $scope.loadDepartCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.departCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCounty = result.data;
                }
            });
    };
    // 根据市编码查询发车区县
    $scope.loadReceiptCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.receiptCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCounty = result.data;
                }
            });
    };

    // 生成运单
    $scope.createWaybill = function () {
        if ($rootScope.checkOrder.length > 0) {
            angular.forEach($rootScope.checkOrder, function (item) {
                item.statusText = Tools.getServiceOrderStatusText(item.status);
            });
            $state.go('waybill-preview', {orders: $rootScope.checkOrder});
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择服务订单',
                timeOut: 3000
            });
        }
    }

    // 查看详情
    $scope.showWaybillModel = function (orderId) {
        WaybillService.selectBySid(orderId)
            .success(function(result){
                if (result.success) {
                    $scope.waybillInfo = result.data;
                    if ($scope.waybillInfo.length > 0) {
                        angular.forEach($scope.waybillInfo, function (item) {
                            item.statusText = Tools.getWaybillStatusText(item.status);
                        });
                    }
                    myOtherAside.$promise.then(myOtherAside.show);
                }
            });
    };
    $scope.checkRow = function(obj) {
        if (obj.order.active) {
            $rootScope.checkOrder.push(obj.order);
            $rootScope.sorderIds.push(obj.order.id);
        } else {
            for (var i = 0; i < $rootScope.checkOrder.length; i++) {
                var item = $rootScope.checkOrder[i];
                if (item.id == obj.order.id) {
                    $rootScope.checkOrder.splice(i, 1);
                    break;
                }
            }
            for (var i = 0; i < $rootScope.sorderIds.length; i++) {
                var item = $rootScope.sorderIds[i];
                if (item == obj.order.id) {
                    $rootScope.sorderIds.splice(i, 1);
                    break;
                }
            }
        }
    };
    // 清空选择的服务订单
    function clearCheckOrder() {
        // 已选择订单列
        $rootScope.checkOrder = [];
        // 合单服务订单IDS
        $rootScope.sorderIds = [];
        // 合单服务订单总价
        $rootScope.sorderCost = 0;
    }

    clearCheckOrder();

    // 加载订单列表数据
    $scope.loadData('load');
    // 加载省数据
    loadProvince();
}])

.controller('SplitVeneerOrderCtrl', ['$rootScope', '$scope', '$state', '$aside', '$modal', 'ServiceVeneerOrderService', 'AreaService', 'WaybillService', 'CompanyService', function ($rootScope, $scope, $state, $aside, $modal, ServiceVeneerOrderService, AreaService, WaybillService, CompanyService) {

    // 查询条件
    $scope.queryOrderinfo = {
        departProvinceCode: '',
        departCityCode: '',
        departCountyCode: '',
        receiptProvinceCode: '',
        receiptCityCode: '',
        receiptCountyCode: '',
        orderCode: '',
        beginCreateTime: '',
        endCreateTime: '',
        sOrderCode: ''
    }

    $scope.waybillInfo = {
        bidderId: ''
    }

    // 订单列表
    $scope.orderInfo = [];

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 加载发车市
    $scope.departCity = [];

    // 加载到达市
    $scope.receiptCity = [];

    // 运单详情
    $scope.wayBillInfo = [];

    // 初始化详情窗体
    var myOtherAside = $aside({scope: $scope, title: '调度详情', templateUrl: $rootScope.Context.path + '/templates/components/order-waybill.html', show: false});

    // 设置模态窗口
    var myOtherModal = $modal({scope: $scope, title: '选择承运商', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/carrier-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        if ($scope.queryOrderinfo.beginCreateTime != null && $scope.queryOrderinfo.endCreateTime != null){
            if ((+$scope.queryOrderinfo.endCreateTime) - (+$scope.queryOrderinfo.beginCreateTime) < 0) {
                $rootScope.hycadmin.toast({
                    title: '开始时间不能大于结束时间',
                    timeOut: 3000
                });
                return;
            }
        }
        ServiceVeneerOrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            departProvinceCode: $scope.queryOrderinfo.departProvinceCode,
            departCityCode: $scope.queryOrderinfo.departCityCode,
            departCountyCode: $scope.queryOrderinfo.departCountyCode,
            receiptProvinceCode: $scope.queryOrderinfo.receiptProvinceCode,
            receiptCityCode: $scope.queryOrderinfo.receiptCityCode,
            receiptCountyCode: $scope.queryOrderinfo.receiptCountyCode,
            orderCode: $scope.queryOrderinfo.orderCode,
            sOrderCode: $scope.queryOrderinfo.sOrderCode,
            beginCreateTime: $scope.queryOrderinfo.beginCreateTime == ''?"":$scope.queryOrderinfo.beginCreateTime.format("yyyy-MM-dd 00:00:00"),
            endCreateTime: $scope.queryOrderinfo.endCreateTime == ''?"":$scope.queryOrderinfo.endCreateTime.format("yyyy-MM-dd 23:59:59")
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.sorders;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.queryOrderinfo = {
            departProvinceCode: '',
            departCityCode: '',
            departCountyCode: '',
            receiptProvinceCode: '',
            receiptCityCode: '',
            receiptCountyCode: '',
            orderCode: '',
            beginCreateTime: '',
            endCreateTime: '',
            sOrderCode: ''
        };
        $scope.loadData('load');
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };
    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.departProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCity = result.data;
                }
            });
    };
    // 根据省编码查询收车市
    $scope.loadReceiptCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.receiptProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCity = result.data;
                }
            });
    };

    // 根据市编码查询收车区县
    $scope.loadDepartCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.departCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCounty = result.data;
                }
            });
    };
    // 根据市编码查询发车区县
    $scope.loadReceiptCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.receiptCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCounty = result.data;
                }
            });
    };

    // 生成运单
    $scope.createWaybill = function (waybillType) {
        if ($rootScope.checkOrder.length == 0) {
            $rootScope.hycadmin.toast({
                title: '请选择服务订单',
                timeOut: 3000
            });
            return;
        } else if ($rootScope.checkOrder.length > 1) {
            $rootScope.hycadmin.toast({
                title: '请选择一条数据',
                timeOut: 3000
            });
            return;
        } else if(waybillType == 20) {
            if ($scope.waybillInfo.bidderId == '' || !Tools.isNumber($scope.waybillInfo.bidderId)) {
                $rootScope.hycadmin.toast({
                    title: '请选择承运商或司机',
                    timeOut: 3000
                });
                return;
            }
        }
        create(waybillType);
    }

    function create(waybillType) {
        WaybillService.createVeneerWaybill({
            serviceOrderId: $rootScope.checkOrder[0].id,
            bidderId: $scope.waybillInfo.bidderId,
            waybillType: waybillType
        }).success(function (result) {
            if (result.success) {
                $rootScope.hycadmin.toast({
                    title: '创建运单成功',
                    timeOut: 3000
                });
                if (waybillType == 20 ) {
                    myOtherModal.$promise.then(myOtherModal.hide);
                }
                $scope.waybillInfo.bidderId = '';
                $scope.loadData('load');
                clearCheckOrder();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 查看详情
    $scope.showWaybillModel = function (orderId) {
        WaybillService.selectBySid(orderId)
            .success(function(result){
                if (result.success) {
                    $scope.waybillInfo = result.data;
                    if ($scope.waybillInfo.length > 0) {
                        angular.forEach($scope.waybillInfo, function (item) {
                            item.statusText = Tools.getWaybillStatusText(item.status);
                        });
                    }
                    myOtherAside.$promise.then(myOtherAside.show);
                }
            });
    };
    $scope.checkRow = function(obj) {
        if (obj.order.active) {
            $rootScope.checkOrder.push(obj.order);
            $rootScope.sorderIds.push(obj.order.id);
        } else {
            for (var i = 0; i < $rootScope.checkOrder.length; i++) {
                var item = $rootScope.checkOrder[i];
                if (item.id == obj.order.id) {
                    $rootScope.checkOrder.splice(i, 1);
                    break;
                }
            }
            for (var i = 0; i < $rootScope.sorderIds.length; i++) {
                var item = $rootScope.sorderIds[i];
                if (item == obj.order.id) {
                    $rootScope.sorderIds.splice(i, 1);
                    break;
                }
            }
        }
    };
    // 清空选择的服务订单
    function clearCheckOrder() {
        // 已选择订单列
        $rootScope.checkOrder = [];
        // 合单服务订单IDS
        $rootScope.sorderIds = [];
        // 合单服务订单总价
        $rootScope.sorderCost = 0;
    }

    // 接单
    $scope.robOrder = function () {
        $scope.createWaybill(10);
    };

    // 派单
    $scope.sendOrder = function () {
        myOtherModal.$promise.then(myOtherModal.show);
        // 初始化承运商
        loadCarrier();
    }

    // 加载承运商
    function loadCarrier() {
        CompanyService.carrierList({
                truckType: 1
            }).success(function(result){
                if (result.success) {
                    $scope.carrierList = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    }

    // 加载订单列表数据
    $scope.loadData('load');
    // 加载省数据
    loadProvince();
}])

.controller('ServiceOrderDeliveryCtrl', ['$rootScope', '$scope', '$state', '$aside', 'ServiceOrderService', 'AreaService', 'WaybillService', function ($rootScope, $scope, $state, $aside, ServiceOrderService, AreaService, WaybillService) {
    // 查询条件
    $scope.queryOrderinfo = {
        departProvinceCode: '',
        departCityCode: '',
        departCountyCode: '',
        receiptProvinceCode: '',
        receiptCityCode: '',
        receiptCountyCode: '',
        orderCode: '',
        beginCreateTime: '',
        endCreateTime: '',
        sOrderCode: ''
    }

    // 订单列表
    $scope.orderInfo = [];

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 加载发车市
    $scope.departCity = [];

    // 加载到达市
    $scope.receiptCity = [];

    // 运单详情
    $scope.wayBillInfo = [];

    // 初始化详情窗体
    var myOtherAside = $aside({scope: $scope, title: '调度详情', templateUrl: $rootScope.Context.path + '/templates/components/order-waybill.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        if ($scope.queryOrderinfo.beginCreateTime != null && $scope.queryOrderinfo.endCreateTime != null){
            if ((+$scope.queryOrderinfo.endCreateTime) - (+$scope.queryOrderinfo.beginCreateTime) < 0) {
                $rootScope.hycadmin.toast({
                    title: '开始时间不能大于结束时间',
                    timeOut: 3000
                });
                return;
            }
        }
        ServiceOrderService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            departProvinceCode: $scope.queryOrderinfo.departProvinceCode,
            departCityCode: $scope.queryOrderinfo.departCityCode,
            departCountyCode: $scope.queryOrderinfo.departCountyCode,
            receiptProvinceCode: $scope.queryOrderinfo.receiptProvinceCode,
            receiptCityCode: $scope.queryOrderinfo.receiptCityCode,
            receiptCountyCode: $scope.queryOrderinfo.receiptCountyCode,
            orderCode: $scope.queryOrderinfo.orderCode,
            sOrderCode: $scope.queryOrderinfo.sOrderCode,
            beginCreateTime: $scope.queryOrderinfo.beginCreateTime == ''?"":$scope.queryOrderinfo.beginCreateTime.format("yyyy-MM-dd 00:00:00"),
            endCreateTime: $scope.queryOrderinfo.endCreateTime == ''?"":$scope.queryOrderinfo.endCreateTime.format("yyyy-MM-dd 23:59:59")
        }).success(function (result) {
            if (result.success) {
                $scope.orderInfo = result.data.sorders;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.queryOrderinfo = {
            departProvinceCode: '',
            departCityCode: '',
            departCountyCode: '',
            receiptProvinceCode: '',
            receiptCityCode: '',
            receiptCountyCode: '',
            orderCode: '',
            beginCreateTime: '',
            endCreateTime: '',
            sOrderCode: ''
        };
        $scope.loadData('load');
    }

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };
    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.departProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCity = result.data;
                }
            });
    };
    // 根据市编码查询发车区县
    $scope.loadDepartCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.departCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCounty = result.data;
                }
            });
    };
    // 根据省编码查询收车市
    $scope.loadReceiptCity = function() {
        AreaService.loadCityByCode($scope.queryOrderinfo.receiptProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCity = result.data;
                }
            });
    };
    // 根据市编码查询收车区县
    $scope.loadReceiptCounty = function() {
        AreaService.loadCountyByCode($scope.queryOrderinfo.receiptCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCounty = result.data;
                }
            });
    };

    // 查看详情
    $scope.showWaybillModel = function (orderId) {
        WaybillService.selectBySid(orderId)
            .success(function(result){
                if (result.success) {
                    $scope.waybillInfo = result.data;
                    if ($scope.waybillInfo.length > 0) {
                        angular.forEach($scope.waybillInfo, function (item) {
                            item.statusText = Tools.getWaybillStatusText(item.status);
                        });
                    }
                    myOtherAside.$promise.then(myOtherAside.show);
                }
            });
    };

    // 确认交车
    $scope.confirmDelivery = function () {
        // 已选择订单列
        $rootScope.checkOrder = [];
        angular.forEach($scope.orderInfo, function(item) {
            if (item.active) {
                $rootScope.checkOrder.push(item.id);
            }
        });
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        if ($rootScope.checkOrder.length > 0) {
            ServiceOrderService.updateServiceStatus({
                    ids: $rootScope.checkOrder.toString(),
                    status: Tools.getOrderStatus.getCompleted()
                }).success(function (result) {
                    if (result.success) {
                        $rootScope.hycadmin.toast({
                            title: '交车成功',
                            timeOut: 3000
                        });
                        $scope.loadData('load');
                    } else {
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 3000
                        });
                    }
                })
                .finally(function() {
                    $rootScope.hycadmin.loading.hide();
                });
        } else {
            $rootScope.hycadmin.toast({
                title: '请选择一个或多个服务订单',
                timeOut: 3000
            });
        }
    };

    // 加载订单列表数据
    $scope.loadData('load');
    // 加载省数据
    loadProvince();
}])

.controller('SubsuppliersCtrl', ['$rootScope', '$scope', 'UserService', function ($rootScope, $scope, UserService) {

    // 查询条件
    $scope.querySub = {
        phone: '',
        name: ''
    };

    $scope.subList = [];

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        UserService.subsupplierList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            phone: $scope.querySub.phone,
            name: $scope.querySub.name
        }).success(function (result) {
            if (result.success) {
                $scope.subList = result.data.users;
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.querySub = {
            name: '',
            phone: ''
        };
        $scope.loadData('load');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    }

    $scope.loadData();
}])

.controller('SubsuppliersEditCtrl', ['$rootScope', '$scope', '$state', 'UserService', function ($rootScope, $scope, $state, UserService) {
    $scope.subTitle = $state.params.subTitle;
    $scope.id = $state.params.id;

    $scope.subObj = {
        name: '',
        realName: '',
        phone: '',
        password: '',
        email: '',
        id: '',
        isEdit: false
    };

    function loadData() {
        if ($scope.id) {
            UserService.selectUserById({
                    id: $scope.id
                }).success(function (result) {
                    if (result.success) {
                        $scope.subObj = result.data;
                        $scope.subObj.isEdit = true;
                    } else {
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 3000
                        });
                    }
                });
        }
    }

    $scope.saveSub = function () {
        if ($scope.subObj.name == null) {
            $rootScope.hycadmin.toast({
                title: '公司名称不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.subObj.realName == null) {
            $rootScope.hycadmin.toast({
                title: '用户名不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.subObj.phone == null) {
            $rootScope.hycadmin.toast({
                title: '手机号不能为空',
                timeOut: 3000
            });
            return;
        } else if (!$scope.subObj.isEdit && $scope.subObj.password == null) {
            $rootScope.hycadmin.toast({
                title: '分供方密码不能为空',
                timeOut: 3000
            });
            return;
        } else if ($scope.subObj.isEdit) {
            if ($scope.subObj.id == null) {
                $rootScope.hycadmin.toast({
                    title: '分供方ID不能为空',
                    timeOut: 3000
                });
                return;
            }
        }
        delete $scope.subObj.createTime;
        delete $scope.subObj.updateTime;
        UserService.modifySubsupplier({
                id: $scope.subObj.id,
                name: $scope.subObj.name,
                realname: $scope.subObj.realName,
                phone: $scope.subObj.phone,
                password: $scope.subObj.password,
                email: $scope.subObj.email
            }).success(function (result) {
                if (result.success) {
                    $state.go('subsupplier-list');
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    };

    loadData();
}])

.controller('StylelicenseCtrl', ['$rootScope', '$scope','$modal', 'StylelicenseService', function ($rootScope, $scope, $modal, StylelicenseService) {

    // 查询列表
    $scope.stylelicenseList = [];

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
        }
        StylelicenseService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            vcstyletype: $scope.vcstyleLicenseParams.vcstyletype
        }).success(function (result) {
                if (result.success) {
                    if (result.data != null) {
                        $scope.stylelicenseList = result.data.styleLicenseList;
                    } else {
                        $scope.styleLicenseList = [];
                    }
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            })
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.vcstyleLicenseParams={
            vcstyletype:''
        };
        $scope.loadData('query');
    };

    $scope.vcstyleLicenseParams={
        vcstyletype:''
    };

    //分页
    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    //编辑／添加－－－－－－－－－－－－－－

    //车型对象
    $scope.styleLicenseObj = {
        id:'',
        vcstylecode: '',
        vcstyletype: '',
        vcstylename: '',
        license: '',
        createTime: '',
        updateTime: ''
    };
    //定义弹出页面
    var styleLicenseModal = $modal({scope: $scope, title: '准驾车型信息维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/stylelicense-edit-view.html', show: false});


    // 显示修改窗口
    $scope.showModel = function (params,license) {
        if ('edit' === params) {
            $scope.styleLicenseObj = license;
            styleLicenseModal.$promise.then(styleLicenseModal.show);
            $scope.isEdit = true;
        } else {
            $scope.isEdit = false;
            // 清空对象
            clearStyleLicense();
            styleLicenseModal.$promise.then(styleLicenseModal.show);
        }
    };
    //删除方法
    $scope.deleteConfim = function (license) {
        $scope.styleLicenseObj = license;
        Tools.layer.confirm("确定要删除车型编号为 "+$scope.styleLicenseObj.vcstylecode+" 的数据么？","删除准驾车型","确定删除","取消",function () {
            detele($scope.styleLicenseObj.id);
        });
    };

    //删除
    function detele(id) {
        StylelicenseService.delete({
            id:$scope.styleLicenseObj.id
        })
            .success(function (result) {
                if (result.success) {
                    clearStyleLicense();
                    $scope.loadData();
                }else{
                    Tools.layer.alert(result.message,"删除准驾车型");
                }
            }).finally(function() {
            $rootScope.hycadmin.loading.hide();
        });
    }

    // 清空准驾车型对象
    function clearStyleLicense() {
        $scope.styleLicenseObj = {
            id:'',
            vcstylecode: '',
            vcstyletype: '',
            vcstylename: '',
            license: '',
            createTime: '',
            updateTime: ''
        };
    };

    //修改数据
    $scope.updateStyleLicense = function () {
        //数据处理  设置更新时间和创建时间为空
        $scope.styleLicenseObj.createTime = null;
        $scope.styleLicenseObj.updateTime = null;
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        StylelicenseService.modify($scope.styleLicenseObj)
            .success(function (result) {
                if (result.success) {
                    styleLicenseModal.$promise.then(styleLicenseModal.hide);
                    clearStyleLicense();
                    $scope.loadData();
                }else{
                    Tools.layer.alert(result.message,"修改准驾车型");
                }
            }).finally(function() {
            $rootScope.hycadmin.loading.hide();
        });

    };
    //添加数据
    $scope.addStyleLicense = function () {
        //数据处理  设置更新时间和创建时间为空
        $scope.styleLicenseObj.createTime = null;
        $scope.styleLicenseObj.updateTime = null;
        $scope.styleLicenseObj.id = null;
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        StylelicenseService.add($scope.styleLicenseObj)
            .success(function (result) {
                if (result.success) {
                    styleLicenseModal.$promise.then(styleLicenseModal.hide);
                    clearStyleLicense();
                    $scope.loadData();
                }else{
                    Tools.layer.alert(result.message,"添加准驾车型");
                }
            }).finally(function() {
            $rootScope.hycadmin.loading.hide();
        });

    };

    $scope.loadData();
}])
