/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

    .controller('LiftTruckListCtrl', ['$rootScope', '$scope', '$state', '$modal', 'AreaService', function ($rootScope, $scope, $state, $modal, AreaService) {
        // 查询条件
        $scope.waybillInfo = {
            departProvinceCode: null,
            departCityCode: null,
            receiptProvinceCode: null,
            receiptCityCode: null,
            beginCreateTime: null,
            endCreateTime: null
        }

        // 查询条件区域数据
        $scope.loadProvince = [];

        // 加载发车市
        $scope.departCity = [];

        // 加载到达市
        $scope.receiptCity = [];

        // 设置模态窗口
        var myOtherModal = $modal({
            scope: $scope,
            title: '提车人信息',
            placement: 'center',
            templateUrl: $rootScope.Context.path + '/templates/components/lift-view.html',
            show: false
        });

        // 加载省数据
        function loadProvince() {
            AreaService.loadProvince()
                .success(function (result) {
                    if (result.success) {
                        $scope.loadProvince = result.data;
                    }
                });
        };

        // 根据省编码查询发车市
        $scope.loadDepartCity = function () {
            AreaService.loadCityByCode($scope.waybillInfo.departProvinceCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.departCity = result.data;
                    }
                });
        };

        // 根据省编码查询收车市
        $scope.loadReceiptCity = function () {
            AreaService.loadCityByCode($scope.waybillInfo.receiptProvinceCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.receiptCity = result.data;
                    }
                });
        }

        // 显示模态窗口
        $scope.showLiftModal = function () {
            myOtherModal.$promise.then(myOtherModal.show);
        }

        // 加载省数据
        loadProvince();
    }])


    .controller('LaborPriceImportCtrl', ['$rootScope', '$scope', '$modal', 'LaborPriceImportService', 'FileUploader', function ($rootScope, $scope, $modal, LaborPriceImportService, FileUploader) {

        var uploader = $scope.uploader = new FileUploader({
            url: '/upload/excel',
            fileName: 'file'
        });
        // a sync filter
        uploader.filters.push({
            name: 'syncFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                // console.log('syncFilter');
                return this.queue.length < 10;
            }
        });
        // CALLBACKS

        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            // console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            fileItem.upload();
            // console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            // console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            // console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            // console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            // console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            // console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            // console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            // console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {

            // console.info('onCompleteItem', fileItem, response, status, headers);
            layer.msg(response.message);
            $scope.laborPriceInfo.batchId = response.data;
            $scope.loadExcelData(response.data);
        };
        uploader.onCompleteAll = function () {
            // console.info('onCompleteAll');
        };

        // console.info('uploader', uploader);

        $scope.laborPriceInfo = {
            id: '',
            routeName: '',
            originProvinceName: '',
            originAreaName: '',
            originCityName: '',
            destProvinceName: '',
            destAreaName: '',
            destCityName: '',
            miles: '',
            currentValue: '',
            validateResultCodeString: '',
            validateResultCode: '',
            effectiveDate: '',
            batchId:''
        };

        function cl() {
            $scope.laborPriceInfo = {
                id: '',
                routeName: '',
                originProvinceName: '',
                originAreaName: '',
                originCityName: '',
                destProvinceName: '',
                destAreaName: '',
                destCityName: '',
                miles: '',
                currentValue: '',
                validateResultCodeString: '',
                validateResultCode: '',
                effectiveDate: '',
                batchId:''
            };
        }

        // 当前对象
        $scope.userTruckObj = {
            pageNo: 1,
            pageSize: 10,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.page.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.page.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadExcelData();
            }
        };

        //
        $scope.loadExcelData = function (data) {
            LaborPriceImportService.loadExcelData({
                pageNo: $scope.userTruckObj.pageNo,
                pageSize: $scope.userTruckObj.pageSize,
                batchId:data
            }).success(function (result) {
                if (result.success) {
                    $scope.laborList = result.data.list;
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    layer.msg(result.message || '数据加载异常');
                }
            });
        };

        function buildPage() {
            $scope.item = [];
            var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5 ? ($scope.userTruckObj.pageNo == 1 ? (parseInt($scope.userTruckObj.pageNo) + 5) : (parseInt($scope.userTruckObj.pageNo) + 5)) : $scope.page.totalPage;
            var start = $scope.userTruckObj.pageNo > 5 ? ($scope.userTruckObj.pageNo == 1 ? (parseInt($scope.userTruckObj.pageNo) - 5) : (parseInt($scope.userTruckObj.pageNo) - 5)) : 1;
            for (var i = start; i <= end; i++) {
                $scope.item.push(i);
            }
        };

        var laborPriceUpdateModal = $modal({
            scope: $scope,
            title: '劳务价格修改',
            placement: 'center',
            templateUrl: $rootScope.Context.path + '/templates/ils/dialog/labor-price-update-view.html',
            show: false
        });

        // 显示修改窗体
        $scope.showUpdateDlg = function (item) {
            cl();
            LaborPriceImportService.getById({
                id: item.id
            }).success(function (result) {
                if (result.success) {
                    $scope.laborPriceInfo = result.data;
                    laborPriceUpdateModal.$promise.then(laborPriceUpdateModal.show);
                } else {
                    layer.msg(result.message || '加载数据异常');
                }
            });
        };

        $scope.update = function () {
            delete $scope.laborPriceInfo.updateTime;
            delete $scope.laborPriceInfo.createTime;
            $scope.laborPriceInfo.effectiveDate = $scope.laborPriceInfo.effectiveDate == '' ? "" : typeof ($scope.laborPriceInfo.effectiveDate) == 'string' ? $scope.laborPriceInfo.effectiveDate : $scope.laborPriceInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
            LaborPriceImportService.update($scope.laborPriceInfo).success(function (result) {
                if (result.success) {
                    $scope.loadExcelData($scope.laborPriceInfo.batchId);
                    laborPriceUpdateModal.$promise.then(laborPriceUpdateModal.hide);
                    layer.msg(result.message || '修改成功');
                } else {
                    layer.msg(result.message || '更新数据异常');
                }
            });
        }


        $scope.del = function (item) {
            var index = layer.confirm('确定删除该数据？', {
                btn: ['确定', '取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '提示'
            }, function () {
                layer.close(index);
                if (item.id == '') {
                    layer.msg('删除标识不能为空');
                    return false;
                }
                LaborPriceImportService.del({id: item.id, batchId:$scope.laborPriceInfo.batchId}).success(function (result) {
                    if (result.success) {
                        layer.msg(result.message || '删除成功');
                        $scope.loadExcelData($scope.laborPriceInfo.batchId);
                    } else {
                        layer.msg(result.message || '删除数据异常');
                    }
                });
            });
        };


        $scope.deleteAll = function () {
            var index = layer.confirm('确定取消导入？', {
                btn: ['确定', '取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '提示'
            }, function () {
                layer.close(index);
                window.location.reload();

                // LaborPriceImportService.deleteAll().success(function (result) {
                //     if (result.success) {
                //         layer.msg(result.message || '取消导入成功');
                //         $scope.loadExcelData();
                //     } else {
                //         layer.msg(result.message || '取消导入异常');
                //     }
                // });
            });
        };

        $scope.importData = function (item) {
            var index = layer.confirm('确定导入？', {
                btn: ['确定', '取消'], //按钮,
                skin: 'admin-confirm-btn-class',
                title: '提示'
            }, function () {
                layer.close(index);
                if (item.validateResult != null) {
                    layer.msg('导入数据有错误不能导入！');
                    return false;
                }
            LaborPriceImportService.importData({batchId:$scope.laborPriceInfo.batchId}).success(function (result) {
                if (result.success) {
                    $scope.loadExcelData($scope.laborPriceInfo.batchId);
                    layer.msg(result.message || '导入成功');
                } else {
                    layer.msg(result.message || '导入数据异常');
                }
            });
            });
        };

    }])
    .controller("LineCtrl",['$rootScope','$scope','AreaService','LineService','$modal',function($rootScope,$scope,AreaService,LineService,$modal){

        //起点区域
        $scope.startArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        //获取省数据
        function loadProvince() {
            AreaService.loadProvince()
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadProvince = result.data;
                        $scope.endLoadProvince = result.data;
                        $scope.startLoadDialogProvince = result.data;
                        $scope.endLoadDialogProvince = result.data;
                    }
                });
        };

        // 起点根据省编码查询发车市
        $scope.startLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.startArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCity = result.data;
                        $scope.startLoadCounty = [];
                        $scope.startArea.cityCode = ''
                        $scope.startArea.countyCode = ''
                    }
                });
        };

        // 起点根据省编码查询发车区县
        $scope.startLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCounty = result.data;
                    }
                });
        };

        // 终点根据省编码查询发车市
        $scope.endLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.endArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCity = result.data;
                        $scope.endLoadCounty = [];
                        $scope.endArea.cityCode = ''
                        $scope.endArea.countyCode = ''
                    }
                });
        };

        // 终点根据省编码查询发车区县
        $scope.endLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCounty = result.data;
                    }
                });
        };

        $scope.page = {
            pageNo: 1,
            totalPage: 0
        };

        // 当前对象
        $scope.userTruckObj = {
            pageNo: 1,
            pageSize: 10,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.page.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.page.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadData();
            },
            startAreaId:'',
            endAreaId:''
        };

        // 初始化数据列表
        $scope.loadData = function (params) {
            if ('query' === params){
                $scope.userTruckObj.pageNo = 1;
                $scope.userTruckObj.pageSize = 10;
                $scope.page.pageNo = 1;
                $scope.page.totalPage = 0;
                if($scope.startArea.proCode && $scope.startArea.cityCode){
                    if($scope.startArea.countyCode){
                        $scope.userTruckObj.startAreaId = $scope.startArea.countyCode;
                    }else{
                        $scope.userTruckObj.startAreaId = $scope.startArea.cityCode;
                    }
                }

                if($scope.endArea.proCode && $scope.endArea.cityCode){
                    if($scope.endArea.countyCode){
                        $scope.userTruckObj.endAreaId = $scope.endArea.countyCode;
                    }else{
                        $scope.userTruckObj.endAreaId = $scope.endArea.cityCode;
                    }
                }
            }
            LineService.list({
                pageNo: $scope.userTruckObj.pageNo,
                pageSize: $scope.userTruckObj.pageSize,
                startAreaId : $scope.userTruckObj.startAreaId,
                endAreaId : $scope.userTruckObj.endAreaId,
                name : $scope.userTruckObj.name ? $scope.userTruckObj.name : ''
            }).success(function (result) {
                if (result.success) {
                    $scope.lineList = result.data.lineList;
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }

        function buildPage () {
            $scope.item = [];
            var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
            var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.item.push(i);
            }
        }

        $scope.clearQuery = function(){
            $scope.startArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };

            $scope.userTruckObj.startAreaId = '';
            $scope.userTruckObj.endAreaId = '';
            $scope.loadData('load');
        }

        //删除线路
        $scope.deleteLine = function(id){
            LineService.remove(id)
                .success(function(result){
                    if(result.success){
                        $rootScope.hycadmin.toast({
                            title: '删除线路成功',
                            timeOut: 2000
                        });
                        $scope.loadData('load');
                    }else{
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 2000
                        });
                    }
                })
        }

        $scope.lineForm = {
            isFactory : 0,
            isChannel : 0
        };

        // 设置模态窗口
        var myOtherModal = $modal({scope: $scope, title: '线路维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/line/line-form.html', show: false});
        //新增线路弹框
        $scope.addLine = function(){
            $scope.lineForm = {
                isFactory : 0,
                isChannel : 0
            };
            //起点区域
            $scope.startAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            }
            $scope.startLoadDialogCity = [];
            $scope.startLoadDialogCounty = [];
            $scope.startLoadDialogCounty = [];

            $scope.endLoadDialogCity = [];
            $scope.endLoadDialogCounty = [];
            $scope.endLoadDialogCounty = [];

            myOtherModal.$promise.then(myOtherModal.show);
        }

        //起点区域
        $scope.startAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        // dialog起点根据省编码查询发车市
        $scope.startLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.startAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCity = result.data;
                        $scope.startLoadDialogCounty = [];
                        $scope.startAreaDialog.cityCode = ''
                        $scope.startAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog起点根据省编码查询发车区县
        $scope.startLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCounty = result.data;
                    }
                });
        };

        // dialog终点根据省编码查询发车市
        $scope.endLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.endAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCity = result.data;
                        $scope.endLoadDialogCounty = [];
                        $scope.endAreaDialog.cityCode = ''
                        $scope.endAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog终点根据省编码查询发车区县
        $scope.endLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCounty = result.data;
                    }
                });
        };

        $scope.modifyLine = function(id){
            LineService.byId(id)
                .success(function(result){
                    if(result.success){
                        $scope.lineForm = result.data;
                        //获取开始区县信息
                        AreaService.getByCode($scope.lineForm.startAreaId)
                            .success(function (result) {
                                if (result.success) {
                                    if(result.data.level == '2'){  //市
                                        $scope.startAreaDialog.proCode = result.data.parentCode;
                                        $scope.startAreaDialog.cityCode = result.data.code;
                                        $scope.startAreaDialog.countyCode = '';
                                        //获取市
                                        AreaService.loadCityByCode(result.data.parentCode)
                                            .success(function (result) {
                                                if (result.success) {
                                                    $scope.startLoadDialogCity = result.data;
                                                    //获取区
                                                    AreaService.loadCountyByCode($scope.startAreaDialog.cityCode)
                                                        .success(function (result) {
                                                            if (result.success) {
                                                                $scope.startLoadDialogCounty = result.data;
                                                            }
                                                        });
                                                }
                                            });
                                    }else if(result.data.level == '3'){  //  区县
                                        $scope.startAreaDialog.cityCode = result.data.parentCode;
                                        $scope.startAreaDialog.countyCode = result.data.code;
                                        //获取 区
                                        AreaService.loadCountyByCode($scope.startAreaDialog.cityCode)
                                            .success(function (result) {
                                                if (result.success) {
                                                    $scope.startLoadDialogCounty = result.data;

                                                    AreaService.getByCode($scope.startAreaDialog.cityCode)
                                                        .success(function(result){
                                                            if(result.success){
                                                                $scope.startAreaDialog.proCode = result.data.parentCode;
                                                                //获取市
                                                                AreaService.loadCityByCode(result.data.parentCode)
                                                                    .success(function (result) {
                                                                        if (result.success) {
                                                                            $scope.startLoadDialogCity = result.data;
                                                                        }
                                                                    });
                                                            }
                                                        })
                                                }
                                            });
                                    }
                                }
                            });
                        //结束区域
                        AreaService.getByCode($scope.lineForm.endAreaId)
                            .success(function (result) {
                                if (result.success) {
                                    if(result.data.level == '2'){  //市
                                        $scope.endAreaDialog.proCode = result.data.parentCode;
                                        $scope.endAreaDialog.cityCode = result.data.code;
                                        $scope.endAreaDialog.countyCode = '';
                                        //获取市
                                        AreaService.loadCityByCode(result.data.parentCode)
                                            .success(function (result) {
                                                if (result.success) {
                                                    $scope.endLoadDialogCity = result.data;
                                                    //获取区
                                                    AreaService.loadCountyByCode($scope.endAreaDialog.cityCode)
                                                        .success(function (result) {
                                                            if (result.success) {
                                                                $scope.endLoadDialogCounty = result.data;
                                                            }
                                                        });
                                                }
                                            });
                                    }else if(result.data.level == '3'){  //  区县
                                        $scope.endAreaDialog.cityCode = result.data.parentCode;
                                        $scope.endAreaDialog.countyCode = result.data.code;
                                        //获取 区
                                        AreaService.loadCountyByCode($scope.endAreaDialog.cityCode)
                                            .success(function (result) {
                                                if (result.success) {
                                                    $scope.endLoadDialogCounty = result.data;

                                                    AreaService.getByCode($scope.endAreaDialog.cityCode)
                                                        .success(function(result){
                                                            if(result.success){
                                                                $scope.endAreaDialog.proCode = result.data.parentCode;
                                                                //获取市
                                                                AreaService.loadCityByCode(result.data.parentCode)
                                                                    .success(function (result) {
                                                                        if (result.success) {
                                                                            $scope.endLoadDialogCity = result.data;
                                                                        }
                                                                    });
                                                            }
                                                        })
                                                }
                                            });
                                    }
                                }
                            });


                        myOtherModal.$promise.then(myOtherModal.show);
                    }else{
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 2000
                        });
                    }
                })
        }

        //更新线路
        $scope.updateLine = function(){
            if(!$scope.lineForm.code){
                $rootScope.hycadmin.toast({
                    title: '请填写线路编码',
                    timeOut: 2000
                });
                return;
            }

            if(!$scope.lineForm.name){
                $rootScope.hycadmin.toast({
                    title: '请填写线路名称',
                    timeOut: 2000
                });
                return;
            }

            //线路类型
            if(!$scope.lineForm.isFactory && !$scope.lineForm.isChannel){
                $rootScope.hycadmin.toast({
                    title: '请选择线路类型',
                    timeOut: 2000
                });
                return;
            }

            if($scope.lineForm.isFactory && $scope.lineForm.isChannel){
                $rootScope.hycadmin.toast({
                    title: '只能选择一种线路类型',
                    timeOut: 2000
                });
                return;
            }

            //开始区域
            if(!$scope.startAreaDialog.proCode || !$scope.startAreaDialog.cityCode){
                $rootScope.hycadmin.toast({
                    title: '请选择开始区域',
                    timeOut: 2000
                });
                return;
            }

            //结束区域
            if(!$scope.endAreaDialog.proCode || !$scope.endAreaDialog.cityCode){
                $rootScope.hycadmin.toast({
                    title: '请选择结束区域',
                    timeOut: 2000
                });
                return;
            }

            //设置开始区域值
            if($scope.startAreaDialog.cityCode && $scope.startAreaDialog.countyCode){
                $scope.lineForm.startAreaId = $scope.startAreaDialog.countyCode;
            }else{
                $scope.lineForm.startAreaId = $scope.startAreaDialog.cityCode;
            }

            //设置结束区域值
            if($scope.endAreaDialog.cityCode && $scope.endAreaDialog.countyCode){
                $scope.lineForm.endAreaId = $scope.endAreaDialog.countyCode;
            }else{
                $scope.lineForm.endAreaId = $scope.endAreaDialog.cityCode;
            }

            if($scope.lineForm.id){
                LineService.update($scope.lineForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '更新线路成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }else{
                LineService.save($scope.lineForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '新增线路成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }
        }

        loadProvince();
        $scope.loadData('load');
    }])
    .controller('LineMileageCtrl',['$rootScope','$scope','AreaService','LineMileageService','LineService','$modal',function($rootScope,$scope,AreaService,LineMileageService,LineService,$modal){
        //起点区域
        $scope.startArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        //获取省数据
        function loadProvince() {
            AreaService.loadProvince()
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadProvince = result.data;
                        $scope.endLoadProvince = result.data;
                        $scope.startLoadDialogProvince = result.data;
                        $scope.endLoadDialogProvince = result.data;
                    }
                });
        };

        // 起点根据省编码查询发车市
        $scope.startLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.startArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCity = result.data;
                        $scope.startLoadCounty = [];
                    }
                });
        };

        // 起点根据省编码查询发车区县
        $scope.startLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCounty = result.data;
                    }
                });
        };

        // 终点根据省编码查询发车市
        $scope.endLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.endArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCity = result.data;
                        $scope.endLoadCounty = [];
                    }
                });
        };

        // 终点根据省编码查询发车区县
        $scope.endLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCounty = result.data;
                    }
                });
        };

        $scope.page = {
            pageNo: 1,
            totalPage: 0
        };

        // 当前对象
        $scope.userTruckObj = {
            pageNo: 1,
            pageSize: 10,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.page.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.page.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadData();
            },
            startAreaId:'',
            endAreaId:''
        };

        $scope.loadData = function (params) {
            if ('query' === params){
                $scope.userTruckObj.pageNo = 1;
                $scope.userTruckObj.pageSize = 10;
                $scope.page.pageNo = 1;
                $scope.page.totalPage = 0;
                if($scope.startArea.proCode && $scope.startArea.cityCode){
                    if($scope.startArea.countyCode){
                        $scope.userTruckObj.startAreaId = $scope.startArea.countyCode;
                    }else{
                        $scope.userTruckObj.startAreaId = $scope.startArea.cityCode;
                    }
                }

                if($scope.endArea.proCode && $scope.endArea.cityCode){
                    if($scope.endArea.countyCode){
                        $scope.userTruckObj.endAreaId = $scope.endArea.countyCode;
                    }else{
                        $scope.userTruckObj.endAreaId = $scope.endArea.cityCode;
                    }
                }
            }
            LineMileageService.list({
                pageNo: $scope.userTruckObj.pageNo,
                pageSize: $scope.userTruckObj.pageSize,
                startAreaId : $scope.userTruckObj.startAreaId,
                endAreaId : $scope.userTruckObj.endAreaId
            }).success(function (result) {
                if (result.success) {
                    $scope.lineMileageList = result.data.priceLineMileageList;
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }

        function buildPage () {
            $scope.item = [];
            var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
            var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.item.push(i);
            }
        }

        $scope.clearQuery = function(){
            $scope.startArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };

            $scope.userTruckObj.startAreaId = '';
            $scope.userTruckObj.endAreaId = '';
            $scope.loadData('load');
        }

        $scope.modifyLineMileage = function(id){
            LineMileageService.byId(id)
                .success(function(result){
                    if(result.success){
                        $scope.lineMileageForm = result.data;
                        LineService.byId($scope.lineMileageForm.lineId)
                            .success(function(res){
                                if(result.success){
                                    $scope.lineList = [];
                                    res.data.select = true;
                                    $scope.lineList.push(res.data);
                                }else{
                                    $rootScope.hycadmin.toast({
                                        title: res.message,
                                        timeOut: 2000
                                    });
                                }
                            })
                        myOtherModal.$promise.then(myOtherModal.show);
                    }else{
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 2000
                        });
                    }
                })
        }

        //起点区域
        $scope.startAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        $scope.lineMileageForm = {};

        // 设置模态窗口
        var myOtherModal = $modal({scope: $scope, title: '线路里程维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/lineMileage/line-mileage-form.html', show: false});

        //添加线路里程
        $scope.addLineMileage = function(){
            $scope.lineMileageForm = {};
            //起点区域
            $scope.startAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            }
            $scope.startLoadDialogCity = [];
            $scope.startLoadDialogCounty = [];
            $scope.startLoadDialogCounty = [];

            $scope.endLoadDialogCity = [];
            $scope.endLoadDialogCounty = [];
            $scope.endLoadDialogCounty = [];

            $scope.lineList = [];
            myOtherModal.$promise.then(myOtherModal.show);
        }

        // dialog起点根据省编码查询发车市
        $scope.startLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.startAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCity = result.data;
                        $scope.startLoadDialogCounty = [];
                        $scope.startAreaDialog.cityCode = ''
                        $scope.startAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog起点根据省编码查询发车区县
        $scope.startLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCounty = result.data;
                    }
                });
        };

        // dialog终点根据省编码查询发车市
        $scope.endLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.endAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCity = result.data;
                        $scope.endLoadDialogCounty = [];
                        $scope.endAreaDialog.cityCode = ''
                        $scope.endAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog终点根据省编码查询发车区县
        $scope.endLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCounty = result.data;
                    }
                });
        };

        $scope.userTruckObjScd = {
            pageNo: 1, //激活时的页码
            pageSize: 5,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.pageScd.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.pageScd.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadDataForm();
            },
            startAreaId:'',
            endAreaId:''
        };

        $scope.pageScd = {
            pageNo: 1,
            totalPage: 0
        };

        $scope.buildPageScd=function(){
            $scope.itemScd = [];
            var end = ($scope.pageScd.totalPage - $scope.userTruckObjScd.pageNo) > 5?($scope.userTruckObjScd.pageNo == 1?(parseInt($scope.userTruckObjScd.pageNo)+5):(parseInt($scope.userTruckObjScd.pageNo)+5)):$scope.pageScd.totalPage;
            var start = $scope.userTruckObjScd.pageNo > 5?($scope.userTruckObjScd.pageNo == 1?(parseInt($scope.userTruckObjScd.pageNo)-5):(parseInt($scope.userTruckObjScd.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.itemScd.push(i);
            }
        };

        // 初始化数据列表
        $scope.loadDataForm = function (params) {
            if ('query' === params){
                $scope.userTruckObjScd.pageNo = 1;
                $scope.userTruckObjScd.pageSize = 10;
                $scope.pageScd.pageNo = 1;
                $scope.pageScd.totalPage = 0;
                if($scope.startAreaDialog.proCode && $scope.startAreaDialog.cityCode){
                    if($scope.startArea.countyCode){
                        $scope.userTruckObjScd.startAreaId = $scope.startAreaDialog.countyCode;
                    }else{
                        $scope.userTruckObjScd.startAreaId = $scope.startAreaDialog.cityCode;
                    }
                }

                if($scope.endAreaDialog.proCode && $scope.endAreaDialog.cityCode){
                    if($scope.endArea.countyCode){
                        $scope.userTruckObjScd.endAreaId = $scope.endAreaDialog.countyCode;
                    }else{
                        $scope.userTruckObjScd.endAreaId = $scope.endAreaDialog.cityCode;
                    }
                }
            }
            LineService.list({
                pageNo: $scope.userTruckObjScd.pageNo,
                pageSize: $scope.userTruckObjScd.pageSize,
                startAreaId : $scope.userTruckObjScd.startAreaId,
                endAreaId : $scope.userTruckObjScd.endAreaId
            }).success(function (result) {
                if (result.success) {
                    $scope.lineList = result.data.lineList;
                    $scope.pageScd = result.data.page;
                    $scope.buildPageScd();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }

        $scope.clearFormQuery = function(){
            $scope.startAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };

            $scope.userTruckObjScd.startAreaId = '';
            $scope.userTruckObjScd.endAreaId = '';
            $scope.loadDataForm('load');
        }

        $scope.updateLineMileage = function(){
            var check = []
            angular.forEach($scope.lineList,function(item){
                if(item.select){
                    check.push(item.id);
                }
            })
            if(check.length == 0){
                $rootScope.hycadmin.toast({
                    title: '请选择一条线路数据',
                    timeOut: 2000
                });
                return;
            }else if(check.length > 1){
                $rootScope.hycadmin.toast({
                    title: '只能选择一条线路数据',
                    timeOut: 2000
                });
                return;
            }

            if(!$scope.lineMileageForm.currentValue){
                $rootScope.hycadmin.toast({
                    title: '请填写里程数',
                    timeOut: 2000
                });
                return;
            }

            //设置线路id
            $scope.lineMileageForm.lineId = check[0];

            if($scope.lineMileageForm.id){
                LineMileageService.update($scope.lineMileageForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '更新线路里程成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }else{
                LineMileageService.save($scope.lineMileageForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '新增线路里程成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }

        }

        loadProvince();
        $scope.loadData('load');
    }])
    .controller('LinePriceSaleOlTpCtrl',['$rootScope','$scope','AreaService','LinePriceSaleOlTpService','LineService','$modal',function($rootScope,$scope,AreaService,LinePriceSaleOlTpService,LineService,$modal){
        //起点区域
        $scope.startArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endArea = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        //获取省数据
        function loadProvince() {
            AreaService.loadProvince()
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadProvince = result.data;
                        $scope.endLoadProvince = result.data;
                        $scope.startLoadDialogProvince = result.data;
                        $scope.endLoadDialogProvince = result.data;
                    }
                });
        };

        // 起点根据省编码查询发车市
        $scope.startLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.startArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCity = result.data;
                        $scope.startLoadCounty = [];
                    }
                });
        };

        // 起点根据省编码查询发车区县
        $scope.startLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadCounty = result.data;
                    }
                });
        };

        // 终点根据省编码查询发车市
        $scope.endLoadDepartCity = function() {
            AreaService.loadCityByCode($scope.endArea.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCity = result.data;
                        $scope.endLoadCounty = [];
                    }
                });
        };

        // 终点根据省编码查询发车区县
        $scope.endLoadDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endArea.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadCounty = result.data;
                    }
                });
        };

        $scope.page = {
            pageNo: 1,
            totalPage: 0
        };

        // 当前对象
        $scope.userTruckObj = {
            pageNo: 1,
            pageSize: 10,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.page.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.page.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadData();
            },
            startAreaId:'',
            endAreaId:''
        };

        $scope.loadData = function (params) {
            if ('query' === params){
                $scope.userTruckObj.pageNo = 1;
                $scope.userTruckObj.pageSize = 10;
                $scope.page.pageNo = 1;
                $scope.page.totalPage = 0;
                if($scope.startArea.proCode && $scope.startArea.cityCode){
                    if($scope.startArea.countyCode){
                        $scope.userTruckObj.startAreaId = $scope.startArea.countyCode;
                    }else{
                        $scope.userTruckObj.startAreaId = $scope.startArea.cityCode;
                    }
                }

                if($scope.endArea.proCode && $scope.endArea.cityCode){
                    if($scope.endArea.countyCode){
                        $scope.userTruckObj.endAreaId = $scope.endArea.countyCode;
                    }else{
                        $scope.userTruckObj.endAreaId = $scope.endArea.cityCode;
                    }
                }
            }
            LinePriceSaleOlTpService.list({
                pageNo: $scope.userTruckObj.pageNo,
                pageSize: $scope.userTruckObj.pageSize,
                startAreaId : $scope.userTruckObj.startAreaId,
                endAreaId : $scope.userTruckObj.endAreaId
            }).success(function (result) {
                if (result.success) {
                    $scope.linePriceSaleOlTpList = result.data.priceSaleOlTpList;
                    $scope.page = result.data.page;
                    buildPage();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }

        function buildPage () {
            $scope.item = [];
            var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
            var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.item.push(i);
            }
        }

        $scope.clearQuery = function(){
            $scope.startArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endArea = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };

            $scope.userTruckObj.startAreaId = '';
            $scope.userTruckObj.endAreaId = '';
            $scope.loadData('load');
        }

        $scope.modifyLinePriceSaleOlTp = function(id){
            LinePriceSaleOlTpService.byId(id)
                .success(function(result){
                    if(result.success){
                        $scope.linePriceSaleOlTpForm = result.data;
                        LineService.byId($scope.linePriceSaleOlTpForm.lineId)
                            .success(function(res){
                                if(result.success){
                                    $scope.lineList = [];
                                    res.data.select = true;
                                    $scope.lineList.push(res.data);
                                }else{
                                    $rootScope.hycadmin.toast({
                                        title: res.message,
                                        timeOut: 2000
                                    });
                                }
                            })
                        myOtherModal.$promise.then(myOtherModal.show);
                    }else{
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 2000
                        });
                    }
                })
        }

        //起点区域
        $scope.startAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };
        //终点区域
        $scope.endAreaDialog = {
            proCode: null,
            proName: null,
            cityCode: null,
            cityName: null,
            countyCode: null,
            countyName: null,
            cityRating: null,
            isTransport: null
        };

        $scope.linePriceSaleOlTpForm = {};

        // 设置模态窗口
        var myOtherModal = $modal({scope: $scope, title: '线路运价维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/linePriceSaleOlTp/line-price-sale-ol-tp-form.html', show: false});

        //添加线路运价
        $scope.addLinePriceSaleOlTp = function(){
            $scope.linePriceSaleOlTpForm = {};
            //起点区域
            $scope.startAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            }
            $scope.startLoadDialogCity = [];
            $scope.startLoadDialogCounty = [];
            $scope.startLoadDialogCounty = [];

            $scope.endLoadDialogCity = [];
            $scope.endLoadDialogCounty = [];
            $scope.endLoadDialogCounty = [];

            $scope.lineList = [];
            myOtherModal.$promise.then(myOtherModal.show);
        }

        // dialog起点根据省编码查询发车市
        $scope.startLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.startAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCity = result.data;
                        $scope.startLoadDialogCounty = [];
                        $scope.startAreaDialog.cityCode = ''
                        $scope.startAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog起点根据省编码查询发车区县
        $scope.startLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.startAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.startLoadDialogCounty = result.data;
                    }
                });
        };

        // dialog终点根据省编码查询发车市
        $scope.endLoadDialogDepartCity = function() {
            AreaService.loadCityByCode($scope.endAreaDialog.proCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCity = result.data;
                        $scope.endLoadDialogCounty = [];
                        $scope.endAreaDialog.cityCode = ''
                        $scope.endAreaDialog.countyCode = ''
                    }
                });
        };

        // dialog终点根据省编码查询发车区县
        $scope.endLoadDialogDepartCounty = function() {
            AreaService.loadCountyByCode($scope.endAreaDialog.cityCode)
                .success(function (result) {
                    if (result.success) {
                        $scope.endLoadDialogCounty = result.data;
                    }
                });
        };

        $scope.userTruckObjScd = {
            pageNo: 1, //激活时的页码
            pageSize: 5,
            loadMore: function ($event, pageNo) {
                this.pageNo = pageNo;
                if ((this.pageNo > $scope.pageScd.totalPage)) {
                    $event.stopPropagation();
                    this.pageNo = $scope.pageScd.totalPage;
                    return;
                } else if (this.pageNo < 1) {
                    $event.stopPropagation();
                    this.pageNo = 1;
                    return;
                }
                $scope.loadDataForm();
            },
            startAreaId:'',
            endAreaId:''
        };

        $scope.pageScd = {
            pageNo: 1,
            totalPage: 0
        };

        $scope.buildPageScd=function(){
            $scope.itemScd = [];
            var end = ($scope.pageScd.totalPage - $scope.userTruckObjScd.pageNo) > 5?($scope.userTruckObjScd.pageNo == 1?(parseInt($scope.userTruckObjScd.pageNo)+5):(parseInt($scope.userTruckObjScd.pageNo)+5)):$scope.pageScd.totalPage;
            var start = $scope.userTruckObjScd.pageNo > 5?($scope.userTruckObjScd.pageNo == 1?(parseInt($scope.userTruckObjScd.pageNo)-5):(parseInt($scope.userTruckObjScd.pageNo)-5)):1;
            for(var i=start; i <= end; i++){
                $scope.itemScd.push(i);
            }
        };

        // 初始化数据列表
        $scope.loadDataForm = function (params) {
            if ('query' === params){
                $scope.userTruckObjScd.pageNo = 1;
                $scope.userTruckObjScd.pageSize = 10;
                $scope.pageScd.pageNo = 1;
                $scope.pageScd.totalPage = 0;
                if($scope.startAreaDialog.proCode && $scope.startAreaDialog.cityCode){
                    if($scope.startArea.countyCode){
                        $scope.userTruckObjScd.startAreaId = $scope.startAreaDialog.countyCode;
                    }else{
                        $scope.userTruckObjScd.startAreaId = $scope.startAreaDialog.cityCode;
                    }
                }

                if($scope.endAreaDialog.proCode && $scope.endAreaDialog.cityCode){
                    if($scope.endArea.countyCode){
                        $scope.userTruckObjScd.endAreaId = $scope.endAreaDialog.countyCode;
                    }else{
                        $scope.userTruckObjScd.endAreaId = $scope.endAreaDialog.cityCode;
                    }
                }
            }
            LineService.listAndMileage({
                pageNo: $scope.userTruckObjScd.pageNo,
                pageSize: $scope.userTruckObjScd.pageSize,
                startAreaId : $scope.userTruckObjScd.startAreaId,
                endAreaId : $scope.userTruckObjScd.endAreaId
            }).success(function (result) {
                if (result.success) {
                    $scope.lineList = result.data.lineList;
                    $scope.pageScd = result.data.page;
                    $scope.buildPageScd();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }

        $scope.clearFormQuery = function(){
            $scope.startAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };
            //终点区域
            $scope.endAreaDialog = {
                proCode: null,
                proName: null,
                cityCode: null,
                cityName: null,
                countyCode: null,
                countyName: null,
                cityRating: null,
                isTransport: null
            };

            $scope.userTruckObjScd.startAreaId = '';
            $scope.userTruckObjScd.endAreaId = '';
            $scope.loadDataForm('load');
        }

        $scope.updateLinePriceSaleOlTp = function(){
            var check = []
            angular.forEach($scope.lineList,function(item){
                if(item.select){
                    check.push(item.id);
                }
            })
            if(check.length == 0){
                $rootScope.hycadmin.toast({
                    title: '请选择一条线路数据',
                    timeOut: 2000
                });
                return;
            }else if(check.length > 1){
                $rootScope.hycadmin.toast({
                    title: '只能选择一条线路数据',
                    timeOut: 2000
                });
                return;
            }

            if(!$scope.linePriceSaleOlTpForm.ltlUnitPrice){
                $rootScope.hycadmin.toast({
                    title: '请填写运价单价',
                    timeOut: 2000
                });
                return;
            }

            if(!$scope.linePriceSaleOlTpForm.ltlGrossPrice){
                $rootScope.hycadmin.toast({
                    title: '请填写运价总价',
                    timeOut: 2000
                });
                return;
            }

            //设置线路id
            $scope.linePriceSaleOlTpForm.lineId = check[0];

            if($scope.linePriceSaleOlTpForm.id){
                LinePriceSaleOlTpService.update($scope.linePriceSaleOlTpForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '更新线路运价成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }else{
                LinePriceSaleOlTpService.save($scope.linePriceSaleOlTpForm)
                    .success(function(result){
                        if(result.success){
                            $rootScope.hycadmin.toast({
                                title: '新增线路运价成功',
                                timeOut: 2000
                            });
                            myOtherModal.$promise.then(myOtherModal.hide);
                            $scope.loadData('load');
                        }else{
                            $rootScope.hycadmin.toast({
                                title: result.message,
                                timeOut: 2000
                            });
                        }
                    })
            }

        }

        //填写单价自动算出总价
        $scope.changePrice = function(){
            var check = []
            angular.forEach($scope.lineList,function(item){
                if(item.select){
                    check.push(item.currentValue);
                }
            })
            if($scope.linePriceSaleOlTpForm.ltlUnitPrice && check.length == 1){
                if(check[0]){
                    $scope.linePriceSaleOlTpForm.ltlGrossPrice = parseFloat($scope.linePriceSaleOlTpForm.ltlUnitPrice) * parseFloat(check[0]);
                }
            }else if(!$scope.linePriceSaleOlTpForm.ltlUnitPrice){
                $scope.linePriceSaleOlTpForm.ltlGrossPrice = '';
            }
        }

        loadProvince();
        $scope.loadData('load');
    }])