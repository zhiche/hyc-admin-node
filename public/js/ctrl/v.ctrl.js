/**
 * Created by Tuffy on 16/6/6.
 */
'use strict';

adminCtrl

.controller('VehicleCtrl', ['$rootScope', '$scope', '$modal', 'VehicleService', function ($rootScope, $scope, $modal, VehicleService) {

    // 车型列表
    $scope.vehiclesList = [];

    // 品牌数据
    $scope.brandList = [];

    // 车系数据
    $scope.vehicleList = [];

    // 级别
    $scope.vehicleClass = [{
        code: 'A',
        name: '请选择'
    },{
        sysVehicleClassid: 1,
        code: 'S',
        name: 'S级'
    },{
        sysVehicleClassid: 2,
        code: 'M',
        name: 'M级'
    },{
        sysVehicleClassid: 3,
        code: 'L',
        name: 'L级'
    },{
        sysVehicleClassid: 4,
        code: 'EX',
        name: 'EX级'
    },];

    //查询条件
    $scope.queryVehicle = {
        brandId: '',
        vehicleId: '',
        sysVehicleClass: ''
    };


    //车系对象
    $scope.vehicleObj = {
        vehicleId: '',
        brandId: '',
        brandName: '',
        brandLogo: '',
        vehicleName: '',
        vehicleClass: '',
        minPrice: '',
        maxPrice: '',
        vehicleLengh: '',
        vehicleWidth: '',
        vehicleHeight: '',
        chassisHeight: '',
        vehicleWeight: '',
        sysVehicleClass: '',
        sysVehicleClassid: ''
    };

    var vehicleModal = $modal({scope: $scope, title: '车系信息维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/vehicle-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        VehicleService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            brandId: $scope.queryVehicle.brandId,
            vehicleId: $scope.queryVehicle.vehicleId,
            sysVehicleClass: $scope.queryVehicle.sysVehicleClass
        }).success(function (result) {
            if (result.success) {
                if (result.data != null) {
                    $scope.vehiclesList = result.data.vehicles;
                } else {
                    $scope.vehiclesList = [];
                }
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };


    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.queryVehicle = {
            brandId: '',
            vehicleId: '',
            sysVehicleClass: ''
        };
        $scope.loadData('load');
    };
    // 清空车系对象
    function clearVehicle() {
        $scope.vehicleObj = {
            vehicleId: '',
            brandId: '',
            brandName: '',
            brandLogo: '',
            vehicleName: '',
            vehicleClass: '',
            minPrice: '',
            maxPrice: '',
            vehicleLengh: '',
            vehicleWidth: '',
            vehicleHeight: '',
            chassisHeight: '',
            vehicleWeight: '',
            sysVehicleClass: '',
            sysVehicleClassid: ''
        }
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    // 加载品牌
    $scope.loadBrand = function () {
        VehicleService.loadBrand()
            .success(function (result) {
                if (result.success) {
                    $scope.brandList = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    };

    // 加载车系
    $scope.loadVehicle = function () {
        VehicleService.loadVehicle($scope.queryVehicle.brandId)
            .success(function (result) {
                if (result.success) {
                    $scope.vehicleList = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    };

    // 显示修改窗口
    $scope.showModel = function (params) {
        if ('edit' === params) {
            $scope.check = [];
            angular.forEach($scope.vehiclesList, function(item) {
                if (item.active) {
                    $scope.check.push(item);
                }
            });
            if ($scope.check.length == 1) {
                $scope.vehicleObj = $scope.check[0];
                vehicleModal.$promise.then(vehicleModal.show);
            } else {
                $rootScope.hycadmin.toast({
                    title: '请选择一条数据',
                    timeOut: 3000
                });
                return;
            }
            $scope.isEdit = true;
        } else {
            $scope.isEdit = false;
            // 清空对象
            clearVehicle();
            vehicleModal.$promise.then(vehicleModal.show);
        }
    };

    //修改数据
    $scope.updateVehicle = function () {
        //数据处理
        if ($scope.vehicleObj.sysVehicleClass != 'A') {
            $scope.vehicleObj.sysVehicleClassid = Tools.getVehicleClass($scope.vehicleObj.sysVehicleClass);
        } else {
            $scope.vehicleObj.sysVehicleClass = '';
            $scope.vehicleObj.sysVehicleClassid = '';
        }
        $scope.vehicleObj.id = $scope.vehicleObj.vehicleId;
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        VehicleService.modify($scope.vehicleObj)
            .success(function (result) {
                if (result.success) {
                    vehicleModal.$promise.then(vehicleModal.hide);
                    clearVehicle();
                    $scope.loadVehicle();
                    $scope.loadData('load');
                }
            }).finally(function() {
                $rootScope.hycadmin.loading.hide();
            });

    };

    // 加载列表数据
    $scope.loadData('load');
    $scope.loadBrand();
}])

.controller('VeneerWaybillCtrl', ['$rootScope', '$scope', '$modal', '$timeout', 'WaybillService', 'AreaService', function ($rootScope, $scope, $modal, $timeout, WaybillService, AreaService) {
    // 查询条件
    $scope.waybillInfo = {
        departProvinceCode: '',
        departCityCode: '',
        departCountyCode: '',
        receiptProvinceCode: '',
        receiptCityCode: '',
        receiptCountyCode: '',
        waybillCode: '',
        beginDateTime: '',
        endDateTime: ''

    };

    // 在途信息对象
    $scope.waybillTrailObj = {
        id: '',
        waybillId: '',
        latitude: '',
        longitude: '',
        cityCode: '',
        cityName: '',
        provinceCode: '',
        provinceName: '',
        countyCode: '',
        countyName: '',
        addr: '',
        updateTime: ''
    };

    // 运单附件对象
    $scope.waybillAttach = {
        id: '',
        userId: '',
        image: '',
        picKey: '',
        category: '',
        type: ''
    };

    // 查询条件区域数据
    $scope.loadProvince = [];

    // 加载发车市
    $scope.departCity = [];

    // 加载到达市
    $scope.receiptCity = [];

    // 加载物流市
    $scope.trailCity = [];

    // 运单列表
    $scope.wayBillList = [];

    // 在途信息列表
    $scope.waybillTrail = [];

    // 运单附件
    $scope.attach = [];

    // 查看在途模态窗口
    var seeRouteModal = $modal({scope: $scope, title: '在途信息', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/see-route-view.html', show: false});

    // 交车审核模态窗口
    var dealAuditModal = $modal({scope: $scope, title: '交车审核', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/components/veneer-deal-audit-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        if ($scope.waybillInfo.beginDateTime != null && $scope.waybillInfo.beginDateTime != null){
            if ((+$scope.waybillInfo.endDateTime) - (+$scope.waybillInfo.beginDateTime) < 0) {
                $rootScope.hycadmin.toast({
                    title: '调度开始时间不能大于结束时间',
                    timeOut: 3000
                });
                return;
            }
        }
        WaybillService.loadWayBill({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            departProvinceCode: $scope.waybillInfo.departProvinceCode,
            departCityCode: $scope.waybillInfo.departCityCode,
            departCountyCode: $scope.waybillInfo.departCountyCode,
            receiptProvinceCode: $scope.waybillInfo.receiptProvinceCode,
            receiptCityCode: $scope.waybillInfo.receiptCityCode,
            receiptCountyCode: $scope.waybillInfo.receiptCountyCode,
            waybillCode: $scope.waybillInfo.waybillCode,
            beginDateTime: $scope.waybillInfo.beginDateTime == ''?"": typeof ($scope.waybillInfo.beginDateTime) == 'string'?$scope.waybillInfo.beginDateTime : $scope.waybillInfo.beginDateTime.format("yyyy-MM-dd 00:00:00"),
            endDateTime: $scope.waybillInfo.endDateTime == ''?"": typeof ($scope.waybillInfo.endDateTime) == 'string'?$scope.waybillInfo.endDateTime : $scope.waybillInfo.endDateTime.format("yyyy-MM-dd 23:59:59"),
            isVeneer: true
        }).success(function (result) {
            if (result.success) {
                $scope.wayBillList = result.data.waybillVo;
                angular.forEach($scope.wayBillList, function (item) {
                    item.statusText = Tools.getWaybillStatusText(item.status);
                })
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.waybillInfo = {
            departProvinceCode: '',
            departCityCode: '',
            departCountyCode: '',
            receiptProvinceCode: '',
            receiptCityCode: '',
            receiptCountyCode: '',
            waybillCode: '',
            status: '',
            payStatus: '',
            beginDateTime: '',
            endDateTime: ''
        };
        $scope.loadData('query');
    };

    // 调整行高度
    $scope.repeatFinish = function () {
        $timeout(function () {
            angular.element(document.getElementsByName("waybillList")).each(function (item) {
                angular.element(this).css({
                    height: angular.element(this).parent().height() + 'px'
                });
                angular.element(this).find('a').css('line-height', angular.element(this).parent().height() - 14 + 'px');
            });
        }, 100);
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
        // 调整行高度
        $scope.repeatFinish();
    };

    // 加载省数据
    function loadProvince() {
        AreaService.loadProvince()
            .success(function (result) {
                if (result.success) {
                    $scope.loadProvince = result.data;
                }
            });
    };
    // 根据省编码查询发车市
    $scope.loadDepartCity = function() {
        AreaService.loadCityByCode($scope.waybillInfo.departProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCity = result.data;
                }
            });
    };
    // 根据市编码查询发车区县
    $scope.loadDepartCounty = function() {
        AreaService.loadCountyByCode($scope.waybillInfo.departCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.departCounty = result.data;
                }
            });
    };
    // 根据省编码查询收车市
    $scope.loadReceiptCity = function() {
        AreaService.loadCityByCode($scope.waybillInfo.receiptProvinceCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCity = result.data;
                }
            });
    };
    // 根据市编码查询收车区县
    $scope.loadReceiptCounty = function() {
        AreaService.loadCountyByCode($scope.waybillInfo.receiptCityCode)
            .success(function (result) {
                if (result.success) {
                    $scope.receiptCounty = result.data;
                }
            });
    };

    // 查看在途上报窗口
    $scope.seeRouteModel = function (waybillId) {
        WaybillTrailService.selectByWaybillId({
            waybillId: waybillId
        }).success(function (result) {
            if (result.success) {
                if (result.data != null) {
                    $scope.waybillTrail = result.data;
                } else {
                    $scope.waybillTrail = [];
                }
                $scope.waybillTrailObj.waybillId = waybillId;
                seeRouteModal.$promise.then(seeRouteModal.show);
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };
    // 交车审核窗口
    $scope.dealAuditModel = function (waybillId) {
        WaybillService.selectWaybillContact({
            id: waybillId,
            attachType: Tools.getAttachType.getDeliver(),
            type: Tools.getContactType.getReceiptPerson()
        }).success(function (result) {
            if (result.success) {
                if (result.data != null) {
                    $scope.contactsObj = result.data;
                }
                $scope.contactsObj.waybillId = waybillId;
                WaybillService.selectWaybillContact({
                    id: waybillId,
                    attachType: Tools.getAttachType.getReceipt(),
                    type: Tools.getContactType.getDepartPerson()
                }).success(function (result) {
                    if (result.success) {
                        $scope.contactsObjReceipt = result.data;
                        dealAuditModal.$promise.then(dealAuditModal.show);
                    } else {
                        $rootScope.hycadmin.toast({
                            title: result.message,
                            timeOut: 3000
                        });
                    }
                });
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 交车审核
    $scope.dealAudit = function () {
        WaybillService.dealAudit({
            waybillId: $scope.contactsObj.id
        }).success(function(result){
            if (result.success) {
                $scope.loadData('load');
                dealAuditModal.$promise.then(dealAuditModal.hide);
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        });
    };

    // 加载订单列表数据
    $scope.loadData('load');
    // 加载省数据
    loadProvince();
}])

.controller('VersionCtrl', ['$rootScope', '$scope', 'VersionService', function ($rootScope, $scope, VersionService) {

    $scope.versionList = [];

    function loadData() {
        VersionService.list()
            .success(function (result) {
                if (result.success) {
                    $scope.versionList = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            })
    }
    $scope.delVersion = function (versionId) {
        VersionService.delVersion({
                id: versionId
            }).success(function (result) {
                if (result.success) {
                    loadData();
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            })
    };

    loadData();
}])

.controller('VersionEditCtrl', ['$rootScope', '$scope', '$state', 'VersionService', function ($rootScope, $scope, $state, VersionService) {
    $scope.versionTitle = $state.params.versionTitle;
    $scope.id = $state.params.id;

    $scope.appType = [
        {
            code: '10',
            name: 'Android'
        },{
            code: '20',
            name: 'IOS'
        }
    ];

    $scope.versionObj = {
        releaseNote: '',
        url: '',
        version: '',
        versionCode: '',
        platform: '',
        appType: '',
        id: '',
        isEdit: false
    };

    function loadData() {
        if ($scope.id) {
            VersionService.getById({
                id: $scope.id
            }).success(function (result) {
                if (result.success) {
                    $scope.versionObj = result.data;
                    $scope.versionObj.isEdit = true;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
        }
    }

    $scope.saveVersion = function () {
        VersionService.modifyVersion($scope.versionObj)
            .success(function (result) {
                if (result.success) {
                    $state.go('version-list');
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
        });
    };

    loadData();
}])
.controller('VcstyleoilCtrl', ['$rootScope', '$scope','$modal', 'VcstyleoilService','DoilpriceService', function ($rootScope, $scope, $modal, VcstyleoilService,DoilpriceService) {

    // 查询列表
    $scope.dVcstyleOilList = [];

    //油品列表
    $scope.oilProductsList = [];

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    //查询对象
    $scope.vcstyleOilParams={
        oilProducts:'',
        name:''
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.vcstyleOilParams={
            oilProducts:'',
            name:''
        };
        $scope.loadData('query');
    };


    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
        }
        VcstyleoilService.list({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            oilProducts: $scope.vcstyleOilParams.oilProducts,
            name: $scope.vcstyleOilParams.name
        }).success(function (result) {
            if (result.success) {
                if (result.data != null) {
                    $scope.dVcstyleOilList = result.data.dVcstyleOilList;
                } else {
                    $scope.dVcstyleOilList = [];
                }
                $scope.page = result.data.page;
                buildPage();
            } else {
                $rootScope.hycadmin.toast({
                    title: result.message,
                    timeOut: 3000
                });
            }
        })
    };

    // 加载油品
    $scope.loadOilProducts = function () {
        DoilpriceService.loadOilProducts()
            .success(function (result) {
                if (result.success) {
                    $scope.oilProductsList = result.data;
                } else {
                    $rootScope.hycadmin.toast({
                        title: result.message,
                        timeOut: 3000
                    });
                }
            });
    };

    //分页
    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    //编辑／添加－－－－－－－－－－－－－－


    //车型对象
    $scope.vcstyleOilObj = {
        code: '',
        name: '',
        oilFuel: '',
        volume: '',
        oilProducts: '',
        oilType: ''
    };
    //定义弹出页面
    var vcstyleOilModal = $modal({scope: $scope, title: '车型油耗维护', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/tms/tms-vcstyleoil-add-view.html', show: false});

    // 显示修改窗口
    $scope.showModel = function (params,vcstyleOil) {
        if ('edit' === params) {
            $scope.vcstyleOilObj = vcstyleOil
            vcstyleOilModal.$promise.then(vcstyleOilModal.show);
            $scope.isEdit = true;
        } else {
            $scope.isEdit = false;
            // 清空对象
            clearVcStyleOil();
            vcstyleOilModal.$promise.then(vcstyleOilModal.show);
        }
    };


    // 清空准驾车型对象
    function clearVcStyleOil() {
        $scope.vcstyleOilObj = {
            code: '',
            name: '',
            oilFuel: '',
            volume: '',
            oilProducts: '',
            oilType: ''
        };
    };

    //修改数据
    $scope.modifyVcStyleOil = function () {
        if($scope.vcstyleOilObj.code == null || $scope.vcstyleOilObj.code == ''){
            Tools.layer.alert("车型编码不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.name == null || $scope.vcstyleOilObj.name == ''){
            Tools.layer.alert("车型名称不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.oilFuel == null || $scope.vcstyleOilObj.oilFuel == ''){
            Tools.layer.alert("油耗不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.volume == null || $scope.vcstyleOilObj.volume == ''){
            Tools.layer.alert("容积不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.oilProducts == null || $scope.vcstyleOilObj.oilProducts == ''){
            Tools.layer.alert("油品不能为空");
            return false;
        }
        $scope.vcstyleOilObj.createTime = null;
        $scope.vcstyleOilObj.updateTime = null;
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        VcstyleoilService.modify($scope.vcstyleOilObj)
            .success(function (result) {
                if (result.success) {
                    vcstyleOilModal.$promise.then(vcstyleOilModal.hide);
                    clearVcStyleOil();
                    $scope.loadData('query');
                }else{
                    Tools.layer.alert(result.message,"修改准驾车型");
                }
            }).finally(function() {
            $rootScope.hycadmin.loading.hide();
        });

    };

    //添加数据
    $scope.addVcStyleOil = function () {
        if($scope.vcstyleOilObj.code == null || $scope.vcstyleOilObj.code == ''){
            Tools.layer.alert("车型编码不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.name == null || $scope.vcstyleOilObj.name == ''){
            Tools.layer.alert("车型名称不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.oilFuel == null || $scope.vcstyleOilObj.oilFuel == ''){
            Tools.layer.alert("油耗不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.volume == null || $scope.vcstyleOilObj.volume == ''){
            Tools.layer.alert("容积不能为空");
            return false;
        }
        if($scope.vcstyleOilObj.oilProducts == null || $scope.vcstyleOilObj.oilProducts == ''){
            Tools.layer.alert("油品不能为空");
            return false;
        }
        $rootScope.hycadmin.loading({
            title: '数据处理中...'
        });
        VcstyleoilService.add($scope.vcstyleOilObj)
            .success(function (result) {
                if (result.success) {
                    vcstyleOilModal.$promise.then(vcstyleOilModal.hide);
                    clearVcStyleOil();
                    $scope.loadData('query');
                }else{
                    Tools.layer.alert(result.message,"添加准驾车型");
                }
            }).finally(function() {
            $rootScope.hycadmin.loading.hide();
        });

    };

    $scope.loadData('query');

    $scope.loadOilProducts();
}])

.controller('VehicleClassifyCtrl', ['$rootScope', '$scope', '$modal', 'CarTypeService', 'FuelService', 'VehicleClassifyService', function ($rootScope, $scope, $modal, CarTypeService, FuelService, VehicleClassifyService) {
    // 查询条件
    $scope.carType = {
        vehicleClassifyId: '',
        vehicleClassifyName: '',
        fuelTypeId: '',
        enable: ''
    };

    $scope.carTypeInfo = {
        id: '',
        vehicleClassifyId: '',
        vehicleClassifyName: '',
        fuelTypeId: '',
        fuelConsumption: '',
        enable: ''
    };

    // 状态
    $scope.enables = [{
        code: false,
        name: '禁用'
    },{
        code: true,
        name: '启用'
    }];

    // 数据列表
    $scope.carTypeList = [];

    // 默认保存
    $scope.saveOrUpdate = true;

    // 驾照类型
    $scope.licenseTypeList = [];

    // 燃油类型
    $scope.fuelTypeList = [];

    var carTypeModal = $modal({scope: $scope, title: '车辆类型', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/ils-car-classify-modify-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        VehicleClassifyService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            vehicleClassifyId: $scope.carType.vehicleClassifyId,
            vehicleClassifyName: $scope.carType.vehicleClassifyName,
            fuelTypeId: $scope.carType.fuelTypeId,
            enable: $scope.carType.enable
        }).success(function (result) {
            if (result.success) {
                $scope.carTypeList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    }

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.carType = {
            vehicleClassifyId: '',
            vehicleClassifyName: '',
            fuelTypeId: '',
            enable: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };

    function cl() {
        $scope.carTypeInfo = {
            id: '',
            vehicleClassifyId: '',
            vehicleClassifyName: '',
            fuelTypeId: '',
            fuelConsumption: '',
            enable: ''
        };
    }

    // 新增及修改
    $scope.saveCarType = function (saveOrUpdate) {
        if (saveOrUpdate) {
            if (validate()) {
                add();
            }
        } else {
            if (validate()) {
                update();
            }
        }
    };

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        $scope.saveOrUpdate = true;
        carTypeModal.$promise.then(carTypeModal.show);
    };

    // 新增
    function add() {
        VehicleClassifyService.add($scope.carTypeInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                carTypeModal.$promise.then(carTypeModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        })
    };

    // 修改
    function update() {
        delete $scope.carTypeInfo.createTime;
        delete $scope.carTypeInfo.updateTime;
        VehicleClassifyService.update($scope.carTypeInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                carTypeModal.$promise.then(carTypeModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        })
    };

    // 修改-dlg
    $scope.showUpdateDlg = function (item) {
        cl();
        VehicleClassifyService.getById({id: item.id}).success(function(result) {
            if (result.success) {
                $scope.carTypeInfo = result.data;
                $scope.saveOrUpdate = false;
                carTypeModal.$promise.then(carTypeModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        })
    };

    // 删除
    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            VehicleClassifyService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });

    };

    // 加载燃油类型
    function loadFuelType() {
        FuelService.list().success(function (result) {
            if (result.success) {
                $scope.fuelTypeList = result.data;
            } else {
                layer.msg(result.message || '初始化燃油类型下拉数据异常');
            }
        })
    }

    // 校验
    function validate() {
        if (!$scope.saveOrUpdate) {
            if ($scope.carTypeInfo.id == '' || $scope.carTypeInfo.id == null) {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.carTypeInfo.vehicleClassifyId == '') {
            layer.msg('分类ID不能为空');
            return false;
        } else if (!Tools.isNumber($scope.carTypeInfo.vehicleClassifyId)) {
            layer.msg('分类ID只能输入数字');
            return false;
        } else if ($scope.carTypeInfo.vehicleClassifyName == '') {
            layer.msg('分类名称不能为空');
            return false;
        } else if (!Tools.unlawfulnessChars($scope.carTypeInfo.vehicleClassifyName)) {
            layer.msg('分类名称不能包含非法字符');
            return false;
        } else if ($scope.carTypeInfo.fuelTypeId == '') {
            layer.msg('燃油类型不能为空');
            return false;
        }
        return true;
    }

    // 启用/禁用
    $scope.enableOrDisabled = function (item, en) {
        VehicleClassifyService.enableOrDisabled({
            id: item.id,
            enableOrDisabled: en
        }).success(function(result){
            if (result.success) {
                $scope.loadData('load');
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        })
    };
    $scope.loadData('load');
    loadFuelType();
}])

.controller('VehicleOcCtrl', ['$rootScope', '$scope', '$modal', 'VehicleOcService', 'VehicleClassifyService', 'FuelService', function ($rootScope, $scope, $modal, VehicleOcService, VehicleClassifyService, FuelService) {
    // 查询条件
    $scope.vehicleOc = {
        vehicleClassifyName: '',
        fuelTypeId: '',
        effectiveDate: '',
        invalidDate: ''
    };

    // 线路对象
    $scope.vehicleOcInfo = {
        id: '',
        vehicleTypeId: '',
        currentValue: '',
        effectiveDate: ''
    };

    // 数据列表
    $scope.vehicleOcList = [];

    $scope.carList = [];

    // 燃油类型
    $scope.fuelTypeList = [];

    var vehicleOcAddModal = $modal({scope: $scope, title: '车型油耗', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/vehicle-oc-add-view.html', show: false});

    var vehicleOcUpdateModal = $modal({scope: $scope, title: '车型油耗', placement: 'center', templateUrl: $rootScope.Context.path + '/templates/ils/dialog/vehicle-oc-update-view.html', show: false});

    // 当前对象
    $scope.userTruckObj = {
        pageNo: 1,
        pageSize: 10,
        loadMore: function ($event, pageNo) {
            this.pageNo = pageNo;
            if ((this.pageNo > $scope.page.totalPage)) {
                $event.stopPropagation();
                this.pageNo = $scope.page.totalPage;
                return;
            } else if (this.pageNo < 1) {
                $event.stopPropagation();
                this.pageNo = 1;
                return;
            }
            $scope.loadData();
        }
    };

    // 初始化数据列表
    $scope.loadData = function (params) {
        if ('query' === params){
            $scope.userTruckObj.pageNo = 1;
            $scope.userTruckObj.pageSize = 10;
            $scope.page.pageNo = 1;
            $scope.page.totalPage = 0;
        }
        VehicleOcService.pageList({
            pageNo: $scope.userTruckObj.pageNo,
            pageSize: $scope.userTruckObj.pageSize,
            vehicleClassifyName: $scope.vehicleOc.vehicleClassifyName,
            fuelTypeId: $scope.vehicleOc.fuelTypeId,
            effectiveDate: $scope.vehicleOc.effectiveDate == ''?"": typeof ($scope.vehicleOc.effectiveDate) == 'string'?$scope.vehicleOc.effectiveDate : $scope.vehicleOc.effectiveDate.format("yyyy-MM-dd 00:00:00"),
            invalidDate: $scope.vehicleOc.invalidDate == ''?"": typeof ($scope.vehicleOc.invalidDate) == 'string'?$scope.vehicleOc.invalidDate : $scope.vehicleOc.invalidDate.format("yyyy-MM-dd 00:00:00")
        }).success(function (result) {
            if (result.success) {
                $scope.vehicleOcList = result.data.list;
                $scope.page = result.data.page;
                buildPage();
            } else {
                layer.msg(result.message || '数据加载异常');
            }
        });
    };

    // 清空查询条件
    $scope.clearQuery = function () {
        $scope.vehicleOc = {
            vehicleClassifyName: '',
            fuelTypeId: '',
            effectiveDate: '',
            invalidDate: ''
        };
        $scope.loadData('query');
    };

    function buildPage () {
        $scope.item = [];
        var end = ($scope.page.totalPage - $scope.userTruckObj.pageNo) > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)+5):(parseInt($scope.userTruckObj.pageNo)+5)):$scope.page.totalPage;
        var start = $scope.userTruckObj.pageNo > 5?($scope.userTruckObj.pageNo == 1?(parseInt($scope.userTruckObj.pageNo)-5):(parseInt($scope.userTruckObj.pageNo)-5)):1;
        for(var i=start; i <= end; i++){
            $scope.item.push(i);
        }
    };


    // 初始化车型
    function loadCarType () {
        VehicleClassifyService.list().success(function (result) {
            if (result.success) {
                $scope.carList = result.data;
            } else {
                layer.msg(result.message || '初始化车型下拉数据异常');
            }
        });
    }

    // 加载燃油类型
    function loadFuelType() {
        FuelService.list().success(function (result) {
            if (result.success) {
                $scope.fuelTypeList = result.data;
            } else {
                layer.msg(result.message || '初始化燃油类型下拉数据异常');
            }
        })
    }

    function cl() {
        $scope.vehicleOcInfo = {
            id: '',
            vehicleTypeId: '',
            currentValue: '',
            effectiveDate: ''
        };
    }

    // 新增-dlg
    $scope.showAddDlg = function () {
        cl();
        vehicleOcAddModal.$promise.then(vehicleOcAddModal.show);
    };

    // 显示修改窗体
    $scope.showUpdateDlg = function(item) {
        cl();
        VehicleOcService.getById({
            id: item.id
        }).success(function (result) {
            if (result.success) {
                $scope.vehicleOcInfo = result.data;
                vehicleOcUpdateModal.$promise.then(vehicleOcUpdateModal.show);
            } else {
                layer.msg(result.message || '加载数据异常');
            }
        });
    };

    // 新增
    $scope.addCarRoute = function (isUpdate) {
        if (isUpdate) {
            if (validate(isUpdate)) {
                update();
            }
        } else {
            if (validate(isUpdate)) {
                add();
            }
        }

    };

    function add() {
        $scope.vehicleOcInfo.effectiveDate = $scope.vehicleOcInfo.effectiveDate == ''?"": typeof ($scope.vehicleOcInfo.effectiveDate) == 'string'?$scope.vehicleOcInfo.effectiveDate : $scope.vehicleOcInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
        VehicleOcService.add($scope.vehicleOcInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                vehicleOcAddModal.$promise.then(vehicleOcAddModal.hide);
            } else {
                layer.msg(result.message || '添加数据异常');
            }
        });
    }

    $scope.del = function (item) {
        var index = layer.confirm('确定删除该数据？', {
            btn: ['确定','取消'], //按钮,
            skin: 'admin-confirm-btn-class',
            title: '提示'
        }, function(){
            layer.close(index);
            if (item.id == '') {
                layer.msg('删除标识不能为空');
                return false;
            }
            VehicleOcService.del({id: item.id}).success(function (result) {
                if (result.success) {
                    $scope.loadData('load');
                } else {
                    layer.msg(result.message || '删除数据异常');
                }
            });
        }, function(){
            //
        });
    };

    function update() {
        delete $scope.vehicleOcInfo.updateTime;
        delete $scope.vehicleOcInfo.createTime;
        $scope.vehicleOcInfo.effectiveDate = $scope.vehicleOcInfo.effectiveDate == ''?"": typeof ($scope.vehicleOcInfo.effectiveDate) == 'string'?$scope.vehicleOcInfo.effectiveDate : $scope.vehicleOcInfo.effectiveDate.format("yyyy-MM-dd 00:00:00");
        VehicleOcService.update($scope.vehicleOcInfo).success(function (result) {
            if (result.success) {
                $scope.loadData('load');
                vehicleOcUpdateModal.$promise.then(vehicleOcUpdateModal.hide);
            } else {
                layer.msg(result.message || '更新数据异常');
            }
        });
    }

    // 校验
    function validate(isUpdate) {
        if (isUpdate) {
            if ($scope.vehicleOcInfo.id == '') {
                layer.msg('主键不能为空');
                return false;
            }
        }
        if ($scope.vehicleOcInfo.vehicleTypeId == '') {
            layer.msg('车型不能为空');
            return false;
        } else if ($scope.vehicleOcInfo.currentValue == '') {
            layer.msg('当前油耗不能为空');
            return false;
        } else if (!Tools.isNumberAndDoubleDigit($scope.vehicleOcInfo.currentValue)) {
            layer.msg('当前油耗只能为数字且为正数（保留两位小数）');
            return false;
        } else if ($scope.vehicleOcInfo.effectiveDate == '') {
            layer.msg('生效时间不能为空');
            return false;
        }
        return true;
    }

    loadCarType();
    loadFuelType();
    $scope.loadData('load');
}])